package com.aforecoppel.afiliaciontrabajador;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sergio Gil (AP Interfaces) on 14/12/18.
 */

class APIClient {

    private static Retrofit retrofit = null;

    static Retrofit getClient(String ipAddress) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl( ipAddress + "/apirestafiliacion/codeigniter/api/ctrlcapturaAfiliacion/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }

}
