package com.aforecoppel.afiliaciontrabajador;

import com.aforecoppel.afiliaciontrabajador.pojo.FolioEnrolamiento;
import com.aforecoppel.afiliaciontrabajador.pojo.TipoConexion;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Sergio Gil (AP Interfaces) on 02/05/19.
 */

interface APIInterface {

    @Headers({
            "Authorization:Basic YWRtaW46MTIzNA==",
            "x-api-key:1234",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("actualizarfolioenrolamiento")
    Call<FolioEnrolamiento> cambiarFolioEnrolamiento(@Field("info") String info);

    @Headers({
            "Authorization:Basic YWRtaW46MTIzNA==",
            "x-api-key:1234",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("actualizarTipoConexion")
    Call<TipoConexion> cambiarConexion(@Field("info") String info);

}
