package com.aforecoppel.afiliaciontrabajador;

public interface AsyncTaskCompleteListener<T> {

    void onTaskComplete(T result, int id);

}
