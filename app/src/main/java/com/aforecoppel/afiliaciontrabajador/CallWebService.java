package com.aforecoppel.afiliaciontrabajador;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.writeLog;

public class CallWebService extends AsyncTask<String, Void, String> {

    private AsyncTaskCompleteListener<String> callback;
    private int idMethod;
    private static String ipAddress;
    private boolean flag;

    CallWebService(String ipAddress){
        CallWebService.ipAddress = ipAddress;
    }

    CallWebService(AsyncTaskCompleteListener<String> callback, int idMethod) {
        this.callback = callback;
        this.idMethod = idMethod;
    }

    public void setFlag(boolean flag){
        this.flag = flag;
    }


    @Override
    protected void onPreExecute(){}

    @Override
    protected String doInBackground(String... params) {

        final MediaType SOAP_MEDIA_TYPE = MediaType.parse("application/xml");
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        RequestBody body = RequestBody.create(SOAP_MEDIA_TYPE, params[0]);

        Response response;
        String xmlMen = "";
        Request request = null;

        try{
            request = new Request.Builder()
                    .url(ipAddress)
                    .post(body)
                    .addHeader("Content-Type", "application/xml")
                    .addHeader("cache-control", "no-cache")
                    .build();
        }catch(IllegalArgumentException | NullPointerException e){
            Log.e("CallWebService", e.toString());
        }


        try{
            response = client.newCall(request).execute();
        }
        catch(IOException e){
            response = null;
        }

        if(response != null){

            if(response.isSuccessful()){

                try {
                    ResponseBody resBody = response.body();
                    if(resBody != null)
                        xmlMen = resBody.string();

                }catch(IOException e){
                    xmlMen = "";
                }
            }
        }

        return xmlMen;


    }

    @Override
    protected void onPostExecute(String xmlMen) {
        callback.onTaskComplete(xmlMen, idMethod);
    }

}
