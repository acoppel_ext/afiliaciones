package com.aforecoppel.afiliaciontrabajador;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapterFinger extends ArrayAdapter<DataFingerModel> implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String[] numExcepcion = {"", "007", "008", "009", "010"};
    private int contExepciones;

    private static class ViewHolder {
        TextView txtFingerName;
        Spinner spFingerException;
    }

    CustomAdapterFinger(Context context, ArrayList<DataFingerModel> data) {
        super(context, R.layout.row_item_finger, data);
    }

    @Override
    public void onClick(View view) {

        int position =(Integer) view.getTag();
        DataFingerModel dataModel = getItem(position);

        switch (view.getId()){
            case R.id.txt_finger_name:

                Snackbar.make(view, "FingerID: " + dataModel.getFingerId() + " Excepcion: " + dataModel.getFingerException(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        if(i != 0) {
            int position =(Integer) adapterView.getTag();
            DataFingerModel dataModel = getItem(position);

            if(dataModel.getFingerException().equals("0"))
                contExepciones++;

            dataModel.setFingerException(numExcepcion[i]);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        DataFingerModel dataModel = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item_finger, parent, false);
            viewHolder.txtFingerName = convertView.findViewById(R.id.txt_finger_name);
            viewHolder.spFingerException = convertView.findViewById(R.id.sp_excepcion);

            convertView.setTag(viewHolder);

        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.spFingerException.setTag(position);
        viewHolder.spFingerException.setOnItemSelectedListener(this);

        viewHolder.txtFingerName.setTag(position);
        viewHolder.txtFingerName.setText(dataModel.getFingerName());
        viewHolder.txtFingerName.setOnClickListener(this);

        return convertView;
    }

    public int getContExepciones(){
        return contExepciones;
    }

}
