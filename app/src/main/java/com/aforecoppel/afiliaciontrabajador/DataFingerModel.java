package com.aforecoppel.afiliaciontrabajador;

import android.os.Parcel;
import android.os.Parcelable;

public class DataFingerModel implements Parcelable {

    private int fingerId;
    private String fingerName;
    private String fingerException;

    public DataFingerModel(int fingerId, String fingerName, String fingerException) {
        this.fingerId=fingerId;
        this.fingerName=fingerName;
        this.fingerException=fingerException;
    }

    public int getFingerId() {
        return fingerId;
    }

    public String getFingerName() {
        return fingerName;
    }

    public void setFingerException(String fingerException){
        this.fingerException = fingerException;
    }

    public String getFingerException() {
        return fingerException;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(fingerId);
        parcel.writeString(fingerName);
        parcel.writeString(fingerException);
    }

    /**
     * Retrieving Movie data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private DataFingerModel(Parcel in){
        this.fingerId = in.readInt();
        this.fingerName = in.readString();
        this.fingerException = in.readString();
    }

    public static final Creator<DataFingerModel> CREATOR = new Creator<DataFingerModel>() {
        @Override
        public DataFingerModel createFromParcel(Parcel source) {
            return new DataFingerModel(source);
        }

        @Override
        public DataFingerModel[] newArray(int size) {
            return new DataFingerModel[size];
        }
    };
}
