package com.aforecoppel.afiliaciontrabajador;

public class FingerArray {

    private FingerObj[] array;
    private int size;

    FingerArray(){
        size = 10;
        array = new FingerObj[size];
    }

    public void newFinger(int id, int nfiq, String base64, String excepcion){
        array[id-1] = new FingerObj(id, nfiq, base64, excepcion);
    }

    public void setTemplate(int pos, String template){
        array[pos].setTemplate(template);
    }

    public void setExcepcion(int pos, String excepcion){
        array[pos].setExcepcion(excepcion);
    }

    public int getSize(){
        return this.size;
    }

    public String getFingersXml(){
        StringBuilder xml = new StringBuilder();
        for(FingerObj finger : array){
            xml.append(finger.getFingerXML());
        }
        return xml.toString();
    }

}
