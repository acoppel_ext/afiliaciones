package com.aforecoppel.afiliaciontrabajador;

public class FingerObj {

    private int fingerid;
    private int nfiq;
    private String base64;
    private String template;
    private String excepcion;

    private static final String etiqueta = "Finger";
    private static final String etiq_idf = "fingerid";
    private static final String etiq_nfi = "nfiq";
    private static final String etiq_b64 = "base64";
    private static final String etiq_tem = "ctemplate";
    private static final String etiq_exc = "excepcion";

    FingerObj(int id, int nfiq, String base64, String excepcion){
        this(id, nfiq, base64, "", excepcion);
    }

    private FingerObj(int id, int nfiq, String base64, String template, String excepcion){
        this.fingerid = id;
        this.nfiq = nfiq;
        this.base64 = base64;
        this.template = template;
        this.excepcion = excepcion;
    }

    public void setFingerid(int id){
        this.fingerid = id;
    }

    public void setNfiq(int nfiq){
        this.nfiq = nfiq;
    }

    public void setBase64(String base64){
        this.base64 = base64;
    }

    public void setTemplate(String template){
        this.template = template;
    }

    public void setExcepcion(String excepcion){
        this.excepcion = excepcion;
    }

    public String getFingerXML(){
        return "<" + etiqueta + ">\n" +
                "\t<"+etiq_idf+">"+fingerid   +"</"+etiq_idf+">\n" +
                "\t<"+etiq_nfi+">"+nfiq       +"</"+etiq_nfi+">\n" +
                "\t<"+etiq_b64+">"+base64     +"</"+etiq_b64+">\n" +
                "\t<"+etiq_tem+">"+template   +"</"+etiq_tem+">\n" +
                "\t<"+etiq_exc+">"+excepcion  +"</"+etiq_exc+">\n" +
               "</" + etiqueta + ">\n";
    }

}
