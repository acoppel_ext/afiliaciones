package com.aforecoppel.afiliaciontrabajador;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.pdf.PdfRenderer;
import android.graphics.pdf.PdfRenderer.Page;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.*;

public class FormatoEnrolActivity extends AppCompatActivity implements AsyncTaskCompleteListener<String>,
        View.OnClickListener{

    private String pdf_name;
    private int folioAfi;
    private int folioSer;
    private String tipo_firma;

    private LinearLayout lyButtons;
    private ImageView pdfView;
    private ImageView signView;
    private Button btnSign;
    private Button btnSave;
    private Button btnAgain;
    private int REQ_WIDTH;

    private String DIR_ACTUAL;
    private String ipAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formato_enrol);

        Intent intent = getIntent();
        getParameters(intent);

        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL + "" + DIR_FORMATO;

        getScreenDimension();

        setComponentes();
        setListeners();

        mostrarPDF();

    }

    public void setComponentes(){

        pdfView = findViewById(R.id.pdfView);
        signView = findViewById(R.id.img_firma);

        lyButtons = findViewById(R.id.ly_buttons_firma);

        btnSign = findViewById(R.id.btn_sign_touch);
        btnSave = findViewById(R.id.btn_sign_save);
        btnAgain = findViewById(R.id.btn_sign_again);

    }

    public void setListeners(){

        /* OnClickListener */
        btnSign.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnAgain.setOnClickListener(this);

    }

    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_FORMATO_PDF)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar), (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        //TOMA LOS VALORES DE LOS PARAMETROS
        try{
            ipAddress   = intent.getStringExtra(CONFIG_KEY1);
            pdf_name    = intent.getStringExtra(ACT_FORMATO_PDF);
            folioAfi       = intent.getIntExtra(ACT_FORMATO_FOAFI, 0);
            folioSer       = intent.getIntExtra(ACT_FORMATO_FOSER, 0);
            tipo_firma  = intent.getStringExtra(ACT_FORMATO_FIRMA);
        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }

    @Override //SE CANCELA CUALQUIER ACCIÓN CON EL BOTÓN DE REGRESAR.
    public void onBackPressed() { }

    public void getScreenDimension(){

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getRealSize(size);
        } catch (NoSuchMethodError err) {
            display.getSize(size);
        }

        REQ_WIDTH = size.x;

    }

    private void mostrarPDF(){

        File directory = new File(DIR_ACTUAL);

        // filePath represent path of Pdf document on storage
        File file = new File(directory, pdf_name);

        // FileDescriptor for file, it allows you to close file when you are done with it
        ParcelFileDescriptor mFileDescriptor = null;
        try {
            mFileDescriptor = ParcelFileDescriptor.open(file,
                    ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (FileNotFoundException e) {
            writeLog("[FormatoEnrolActivity][mostrarPDF] FileNotFoundException"  + e.getStackTrace());

            e.printStackTrace();
        }

        // PdfRenderer enables rendering a PDF document
        PdfRenderer mPdfRenderer = null;
        try {
            assert mFileDescriptor != null;
            mPdfRenderer = new PdfRenderer(mFileDescriptor);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        // Open page with specified index
        assert mPdfRenderer != null;
        Page mCurrentPage = mPdfRenderer.openPage(0);

        int pageWidth = mCurrentPage.getWidth();
        int pageHeight = mCurrentPage.getHeight();
        float scale = (float) REQ_WIDTH / pageWidth;

        pageWidth  = (int) (pageWidth * scale);
        pageHeight = (int) (pageHeight * scale);

        Bitmap bitmap = Bitmap.createBitmap(pageWidth, pageHeight,
                Bitmap.Config.ARGB_8888);

        // Pdf page is rendered on Bitmap
        mCurrentPage.render(bitmap, null, null,
                Page.RENDER_MODE_FOR_DISPLAY);
        // Set rendered bitmap to ImageView (pdfView in my case)
        pdfView.setImageBitmap(bitmap);

        mCurrentPage.close();
        mPdfRenderer.close();
        try {
            mFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == btnSign.getId() || view.getId() == btnAgain.getId()){
            MessageDialog aviso = new MessageDialog();
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_firma_formato));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) ->
                            llamaComponenteFirma());
            showMessage(this, aviso);


        }
        else if(view.getId() == btnSave.getId()){
            btnSave.setEnabled(false);
            btnAgain.setEnabled(false);
            soapMoverImagenFirma();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        MessageDialog aviso = new MessageDialog();

        if (requestCode == LLAMAR_FIRMA_ENROLA) {

            if(resultCode == Activity.RESULT_OK){

                if(data.hasExtra(ACT_FIRMA_NOMFIR)){
                    lyButtons.setVisibility(View.VISIBLE);
                    btnSign.setVisibility(View.GONE);
                    Bundle firma = data.getExtras();
                    String nameFirma = "";
                    if (firma != null) {
                        nameFirma = firma.getString(ACT_FIRMA_NOMFIR);
                    }
                    writeLog("nameFirma " + nameFirma );
                    setSignImage(nameFirma);
                }
                else{
                   Toast.makeText(this,  R.string.msn_can_firma, Toast.LENGTH_LONG).show();
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();
            }
        }
    }//onActivityResult

    @Override
    public void onTaskComplete(String xmlMen, int id) {
        MessageDialog aviso;

        if(xmlMen.equals("")){
            aviso = new MessageDialog();
            aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_ser_res));
            showMessage(this, aviso);
            return;
        }
        HashMap<String, String> values = getValuesXML(xmlMen);

        if(values == null){
            aviso = new MessageDialog();
            aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_ser_xml));
            showMessage(this, aviso);
            return;
        }

        int iEstatus = Integer.parseInt(values.get("Estatus"));
        //String esMensaje = values.get("EsMensaje");

        //SE VALIDA DE CUAL LLAMADO VUELVE EL WEBSERVICE
        switch (id) {
            case SOAP_MOVER_IMG_FIRMA:

                if(iEstatus == 200 || iEstatus == 201){
                    aviso = new MessageDialog();
                    aviso.setTitle(getResources().getString(R.string.msn_avi_enroltrab));
                    aviso.setMensaje(getResources().getString(R.string.msn_avi_enroltrab_men));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) -> {
                        showToast(this, getResources().getString(R.string.msn_avi_firma_pub));
                        setResult(Activity.RESULT_OK);
                        finish();
                    });
                    showMessage(this, aviso);

                }else{
                    aviso = new MessageDialog();
                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_firma));
                    showMessage(this, aviso);
                    btnSave.setEnabled(true);
                    btnAgain.setEnabled(true);
                }
                break;
        }
    }

    public void llamaComponenteFirma(){
        Intent signDocumentIntent = new Intent(Intent.ACTION_EDIT, Uri.parse("myapp://firmadigital.aforecoppel.com"));

        Bundle extra = new Bundle();
        extra.putString(CONFIG_KEY1, ipAddress);
        extra.putInt(ACT_FIRMA_FOAFI, folioAfi);
        extra.putInt(ACT_FIRMA_FOSER, folioSer);
        extra.putString(ACT_FIRMA_TIPOF, tipo_firma);
        signDocumentIntent.putExtras(extra);

        startActivityForResult(signDocumentIntent, LLAMAR_FIRMA_ENROLA);
    }

    public void soapMoverImagenFirma(){
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "MoverImagenFirma");
        sSoap.setParams("iOpcion", (folioSer != 0 ? "2" : "1"));
        sSoap.setParams("iFolio", (folioSer != 0 ? folioSer + "" : folioAfi + ""));

        writeLog("[MoverImagenFirma] -> iOpcion: [" + (folioSer != 0 ? "2" : "1") + "] \n" +
                "iFolio = [" + (folioSer != 0 ? folioSer + "" : folioAfi + "") + "]");

        CallWebService callWS = new CallWebService(this, SOAP_MOVER_IMG_FIRMA);
        callWS.execute(sSoap.getSoapString());
    }

    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public void setSignImage(String signPath){
        signView.setVisibility(View.VISIBLE);

        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL + folioAfi + "/" + DIR_FORMATO;

        signPath = DIR_ACTUAL + signPath;
        writeLog("signPath " + signPath);
        Bitmap myBitmap = BitmapFactory.decodeFile(signPath);
        try{

            int imgW = myBitmap.getWidth();
            writeLog("imgW " + imgW);
            int imgH = myBitmap.getHeight();
            writeLog("imgH " + imgH);
            int originalDP = dpToPx(200);
            writeLog("originalDP " + originalDP);

            double scale = imgW / originalDP;

            imgW = (int) (imgW / scale);
            imgH = (int) (imgH / scale);

            signView.setImageBitmap(Bitmap.createScaledBitmap(myBitmap, imgW, imgH, false));

        }catch (Exception e){
            writeLog("[FormatoEnrolActivity][Exception]  DIR_ACTUAL" + e.getStackTrace());
        }

    }

    public int dpToPx(int dp) {
        float density = this.getApplicationContext().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

}