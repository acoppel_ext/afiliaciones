package com.aforecoppel.afiliaciontrabajador;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.aforecoppel.afiliaciontrabajador.pojo.FolioEnrolamiento;
import com.aforecoppel.afiliaciontrabajador.pojo.infoFolio;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.*;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_AFILIACION_CEN;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_AFILIACION_NOM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_AFILIACION_NUM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.CONFIG_KEY1;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_AFILIACION;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_DIGITAL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_EXTERNA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_HUELLAS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_LOGS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_REENROL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_TEMP;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_WEB_AFILIACION;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_WEB_CAPTURA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_WEB_IMPRESION;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.PERMISSIONS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.PERMISSION_ALL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.showMessage;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button boton_constancia_afiliacion, boton_captura_afiliacion;
    private String DIR_ACTUAL;
    private String todayDate;
    private TextView txtPromCen;
    private TextView txtPromNom;
    private Integer iEmpleado;
    private String ipAddress;
    private Spinner spinnerM;
    private TextView tvVersion;
    private String ipOffline = "";
    private String ipwshuellashc = "";
    private String Version = BuildConfig.VERSION_NAME;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            trimCache(this);
        } catch (Exception e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }

        Date sysDate = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
        todayDate = dateFormat.format(sysDate);

        setComponentes(); // Obtener los componentes en las variables de los componentes
        setListeners(); //Asignar los listeners a los componentes que los necesiten

        //VALORES QUE LLEGAN DEL MENÚ
        Intent intent = getIntent();
        getParameters(intent);

        //UNA VEZ QUE ESTAN LOS COMPONENTES SE PIDE PERMISO
        if(hasPermissions(PERMISSIONS) && android.os.Build.VERSION.SDK_INT >= 23){
            requestPermissions(PERMISSIONS, PERMISSION_ALL);
        }

        //CREAR DIRECTORIOS
        directorios();
        ObtenerVersionesApks();
        tvVersion.setText("V "+Version);
        tvVersion.setText("V "+Version);tvVersion.setText("V "+Version);


    }

    public void ObtenerVersionesApks(){
        PackageInfo pinfo;
        try {

            String[] packapks = {PAQMOD,PAQFIR,PAQDIG,PAQAFI, PAQVID};
            String[] NombresApks = {NAME_MODULO+"\t",NAME_FIRMA_ENROLA+"\t\t\t\t",NAME_DIGITALIZADOR+"\t\t\t",NAME_AFILIACION+"\t", NAME_VIDEO+"\t\t\t\t"};
            String[] arraySpinner = new String[packapks.length];

            for (int NumApk = 0; NumApk < packapks.length; NumApk++){
                pinfo = getPackageManager().getPackageInfo(packapks[NumApk], 0);
                arraySpinner[NumApk] = NombresApks[NumApk] +"\t"+ pinfo.versionName;
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, arraySpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerM.setAdapter(adapter);
            }
        } catch (PackageManager.NameNotFoundException e) {
            writeLog("VERSIONES: "+e.getMessage());
        }
    }

    public void setComponentes(){
        //COMPONENTES DE CABECERA PARA MOSTRAR NOMBRE
        txtPromCen  = findViewById(R.id.txt_tienda);
        txtPromNom  = findViewById(R.id.txt_name);
        spinnerM = findViewById(R.id.spinner3);
        tvVersion = findViewById(R.id.tvVersionM);
        //COMPONENTES DEL SUB-MENU DE AFILIACION
        boton_constancia_afiliacion  = findViewById(R.id.constancia_afiliacion);
        boton_captura_afiliacion  = findViewById(R.id.captura_afiliacion);

    }

    public void setListeners(){
        /*ON CLICK LISTENER*/
        boton_constancia_afiliacion.setOnClickListener(this);
        boton_captura_afiliacion.setOnClickListener(this);
    }

    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }

    protected void directorios() {
        File directoryAfiliacion= new File(DIR_EXTERNA, DIR_AFILIACION);

        if(!directoryAfiliacion.exists()) {
            if (directoryAfiliacion.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryAfiliacion");
            }
        }

        DIR_ACTUAL = DIR_EXTERNA + DIR_AFILIACION;
        File directoryLogs= new File(DIR_ACTUAL, DIR_LOGS);
        File directoryTemp= new File(DIR_ACTUAL, DIR_TEMP);

        if(!directoryLogs.exists()) {
            if (directoryLogs.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryLogs");
            }
        }

        if(!directoryTemp.exists()) {
            if (directoryTemp.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryTemp");
            }
        }

        /* LOG para Aplicaciones DIGITALIZADOR, FIRMA DIGITAL */

        File directoryReenrolamiento= new File(DIR_EXTERNA, DIR_REENROL);

        if(!directoryReenrolamiento.exists()) {
            if (directoryReenrolamiento.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryReenrolamiento");
            }
        }

        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
        File directoryDigital= new File(DIR_ACTUAL, DIR_DIGITAL);
        File directoryHuellas= new File(DIR_ACTUAL, DIR_HUELLAS);
        File directoryLogsReenrol= new File(DIR_ACTUAL, DIR_LOGS);
        File directoryTempReenrol= new File(DIR_ACTUAL, DIR_TEMP);

        if(!directoryDigital.exists()) {
            if (directoryDigital.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryDigital");
            }
        }

        if(!directoryHuellas.exists()) {
            if (directoryHuellas.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryHuellas");
            }
        }

        if(!directoryLogsReenrol.exists()) {
            if (directoryLogsReenrol.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryLogsReenrol");
            }
        }

        if(!directoryTempReenrol.exists()) {
            if (directoryTempReenrol.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada directoryTempReenrol");
            }
        }
    }

    private boolean hasPermissions(String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            for (String permission : permissions) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {
       if(view.getId() == boton_constancia_afiliacion.getId()){
            llamaWebAfiliacion();
        }

        if(view.getId() == boton_captura_afiliacion.getId()){
            llamaWebCaptura();
        }
    }

    private void llamaWebAfiliacion(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName(this, WebVisorAfiliacion.class));

        Bundle extra = new Bundle();
        extra.putString(CONFIG_KEY1, ipAddress);
        extra.putString("ipOffline", ipOffline);
        extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
        extra.putString("ipwshuellashc", ipwshuellashc);
        intent.putExtras(extra);

        startActivityForResult(intent, LLAMAR_WEB_AFILIACION);
    }

    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            ipAddress = ipAddress.replace("20307","50131");
            ipOffline = intent.getStringExtra("ipOffline");
            iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);
            txtPromNom.setText(intent.getStringExtra(ACT_AFILIACION_NOM));
            txtPromCen.setText(intent.getStringExtra(ACT_AFILIACION_CEN));
            ipwshuellashc = intent.getStringExtra("ipwshuellashc");

        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }

    private void llamaWebCaptura(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName(this, WebVisorCaptura.class));

        Bundle extra = new Bundle();
        extra.putString(CONFIG_KEY1, ipAddress);
        extra.putString("ipOffline", ipOffline);
        extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
        intent.putExtras(extra);

        startActivityForResult(intent, LLAMAR_WEB_CAPTURA);
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            trimCache(this);
        } catch (Exception e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        //The directory is now empty so delete it
        return dir.delete();
    }
}
