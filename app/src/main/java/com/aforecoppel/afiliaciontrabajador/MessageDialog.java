package com.aforecoppel.afiliaciontrabajador;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Html;

public class MessageDialog extends DialogFragment {

    private String sTitle;
    private String sMensaje;
    private String sPositive;
    private String sNegative;

    private DialogInterface.OnClickListener accionPositive;
    private DialogInterface.OnClickListener accionNegative;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if(sTitle != null)
            builder.setTitle(sTitle);

        if(sMensaje != null)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                builder.setMessage(Html.fromHtml(sMensaje, Html.FROM_HTML_MODE_LEGACY));
            } else {
                builder.setMessage(Html.fromHtml(sMensaje));
            }

        if(sPositive != null)
            builder.setPositiveButton(sPositive, accionPositive);

        if(sNegative != null)
            builder.setNegativeButton(sNegative, accionNegative);

        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);

        return alert;
    }

    public void setTitle(String sTitle){
        this.sTitle = sTitle;
    }

    public void setMensaje(String sMensaje){
        this.sMensaje = sMensaje;
    }

    private void setPositiveButton(String sPositive){
        this.sPositive = sPositive;
    }

    private void setNegativeButton(String sNegative){
        this.sNegative = sNegative;
    }

    public void setPositiveButton(String sPositive, DialogInterface.OnClickListener accionPositive){
        setPositiveButton(sPositive) ;
        this.accionPositive = accionPositive;
    }

    public void setNegativeButton(String sNegative, DialogInterface.OnClickListener accionNegative){
        setNegativeButton(sNegative);
        this.accionNegative = accionNegative;
    }

    public void setDefaultAceptar(String sMensaje){
        this.sPositive = "Aceptar";
        this.sMensaje = sMensaje;

        this.accionPositive = (dialogInterface, i) -> dialogInterface.dismiss();
    }

    public void setDefaultAceptarCancelar(String sMensaje, DialogInterface.OnClickListener accionPositive){
        this.sPositive = "Aceptar";
        this.sNegative = "Cancelar";
        this.sMensaje = sMensaje;

        this.accionPositive = accionPositive;
        this.accionNegative = (dialogInterface, i) -> dialogInterface.dismiss();
    }

}
