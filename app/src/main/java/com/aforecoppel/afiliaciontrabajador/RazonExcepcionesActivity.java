package com.aforecoppel.afiliaciontrabajador;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.showToast;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.writeLog;

public class RazonExcepcionesActivity extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<DataFingerModel> dataModels;
    private static CustomAdapterFinger adapter;
    private ListView listView;
    private Button btnAceptar;
    private Button btnCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_excepciones);

        //VALORES QUE LLEGAN DEL MENÚ
        Intent intent = getIntent();
        getParameters(intent);

        setComponentes();//Obtener los componentes en las variables de los componentes
        setListeners();//Asignar los listeners a los componentes que los necesiten

        adapter= new CustomAdapterFinger(this, dataModels);
        listView.setAdapter(adapter);

    }

    public void getParameters(Intent intent){
        Bundle args = intent.getBundleExtra("BUNDLE");
        dataModels = (ArrayList<DataFingerModel>) args.getSerializable("ARRAYLIST");
    }

    public void setComponentes(){
        listView  = findViewById(R.id.list_excepciones);
        btnAceptar = findViewById(R.id.btn_ex_aceptar);
        btnCancelar = findViewById(R.id.btn_ex_cancelar);
    }

    public void setListeners(){
        btnAceptar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.btn_ex_aceptar:

                if(adapter.getContExepciones() < dataModels.size())
                    showToast(this,
                            "Promotor, favor de seleccionar una opción valida para todas las excepciones");
                else{
                    Intent i = new Intent();
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST", dataModels);
                    i.putExtra("BUNDLE",args);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }

                break;

            case R.id.btn_ex_cancelar:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
        }

    }
}
