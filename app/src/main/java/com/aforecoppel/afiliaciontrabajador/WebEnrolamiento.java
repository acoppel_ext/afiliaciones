package com.aforecoppel.afiliaciontrabajador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.aforecoppel.afiliaciontrabajador.pojo.FolioEnrolamiento;
import com.aforecoppel.afiliaciontrabajador.pojo.TipoConexion;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.*;
import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;
import com.aforecoppel.afiliaciontrabajador.pojo.infoConexion;
import com.aforecoppel.afiliaciontrabajador.pojo.infoFolio;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebEnrolamiento extends AppCompatActivity implements AsyncTaskCompleteListener<String>{
    APIInterface apiInterface;
    private WebView mWebViewEnrolamiento;
    private int opcionManos; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACION
    private String DIR_ACTUAL;
    private int opcionHuellas;
    private String opcionFirmas;
    private int iEmpleado;
    private int iFolioSolicitud;
    private String todayDate;
    private String txtCurpCliente;
    private  Integer iTipoSolicitud;
    private int iFolioEnrolamiento = 0;
    private int iContadorEnrol;
    private String ipServidor;
    private int iFolioServicio = 0;
    private int iTipoConexion = 1;
    private boolean flagReenrol = false;
    private boolean habilitarBack = false;
    //COMPONENTES DE CABECERA PARA MOSTRAR NOMBRE

    private String ipAddress;
    private  String ipOffline = "";

    private FingerArray arrFingers;
    private ArrayList<DataFingerModel> dataFigers;

    Activity activity;

    //VARIABLES DE LA ACTIVIDAD
    private RequestQueue queue;

    private String datosTemplate;
    private JSONObject fingerEmpleado;
    private static String template;
    private boolean flagSoap = true;
    private static String ip;
    private static String ipwshuellashc;
    private static String numdedo;
    private static int contMatch;
    private static int contCall;
    private static int contTotal;
    private static boolean flagError;
    private static boolean flagProm;
    private String MensajeLog = "Default";
    private String EstatusLog = "Afiliacion Movil - WebEnrolamiento:";
    private static String imagen;
    private static String nist;
    private static String fap = "50";
    private static String dispositivo;
    private static int contPromHuella;
    private int lvlInternet;
    public String ipAddressWsH;


    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_enrolamiento);
        iContadorEnrol = 0;

        queue = Volley.newRequestQueue(this);

        NukeSSLCerts ObjectNukeSSL = new NukeSSLCerts();
        ObjectNukeSSL.nuke();

        if (!isOnline()) {

            Intent intent = new Intent(this, errorConexion.class);
            this.startActivity(intent);

        } else {

            try{

                Bundle info;
                info = getIntent().getExtras();
                iEmpleado = info.getInt("empleado");
                txtCurpCliente = info.getString("curl");
                iTipoSolicitud = info.getInt("tiposolicitud");
                iFolioSolicitud = info.getInt("folio");
                ipAddress = info.getString(CONFIG_KEY1);
                ipOffline = info.getString("ipOffline");
                ipwshuellashc = info.getString("ipwshuellashc");

                directorios();

                apiActualizarConexion(iFolioSolicitud+"");

                new CallWebService(ipAddress);

            }
            catch (NullPointerException e){
                writeLog("onCreate CallWebService Error: " + e.getStackTrace());

            }

            Date sysDate = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
            todayDate = dateFormat.format(sysDate);

            mWebViewEnrolamiento = (WebView) findViewById(R.id.webviewenrolamiento);
            WebSettings mWebSettings = mWebViewEnrolamiento.getSettings();

            mWebSettings.setJavaScriptEnabled(true);
            JavaScriptInterface jsInterface = new JavaScriptInterface();
            mWebViewEnrolamiento.addJavascriptInterface(jsInterface, "Android");

            /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
            ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);*/
            mWebViewEnrolamiento.loadUrl(ipOffline + "/enrolamientotrabajador/indexenroltrabajador.html?tiposolicitud=" + iTipoSolicitud + "&curp=" + txtCurpCliente + "&foliosolicitud=" + iFolioSolicitud +"&empleado=" + iEmpleado);

        }

    }

    public void directorios(){
        /* AFILIACION */
        DIR_ACTUAL = DIR_EXTERNA + DIR_AFILIACION;
        File directory= new File(DIR_ACTUAL, iFolioSolicitud+"");

        if(!directory.exists()) {
            if (directory.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada Folio");
            }
        }

        File directoryDigitalizador= new File(DIR_ACTUAL + "/" + iFolioSolicitud , "Digitalizador");

        if(!directoryDigitalizador.exists()) {
            if (directoryDigitalizador.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada Folio");
            }
        }

        File directoryFormatoEnrolamiento= new File(DIR_ACTUAL + "/" + iFolioSolicitud , "FormatoEnrolamiento");

        if(!directoryFormatoEnrolamiento.exists()) {
            if (directoryFormatoEnrolamiento.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada Folio");
            }
        }
        /* REENROLAMIENTO */
        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
        File directoryR= new File(DIR_ACTUAL, iFolioSolicitud+"");

        if(!directoryR.exists()) {
            if (directoryR.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada Folio");
            }
        }

        File directoryDigitalizadorR= new File(DIR_ACTUAL + "/" + iFolioSolicitud , "Digitalizador");

        if(!directoryDigitalizadorR.exists()) {
            if (directoryDigitalizadorR.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada Folio");
            }
        }

        File directoryFormatoEnrolamientoR= new File(DIR_ACTUAL + "/" + iFolioSolicitud , "FormatoEnrolamiento");

        if(!directoryFormatoEnrolamientoR.exists()) {
            if (directoryFormatoEnrolamientoR.mkdirs()) {
                Log.i("MkDirs", "Carpeta creada Folio");
            }
        }

    }

    @SuppressLint("JavascriptInterface")
    public class JavaScriptInterface {

        @JavascriptInterface
        public void llamaComponenteHuellas(String tipoCaptura, String identificador, String band, String opcion) {

            /* DIRECTORIOS DE COMPLEMENTOS */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, identificador+"");
            //iFolioSolicitud = identificador;//1001234;identificador
            iFolioEnrolamiento = Integer.parseInt(identificador);
            opcionManos = Integer.parseInt(tipoCaptura);

            /* Opcion de Huellas */
            opcionHuellas=Integer.parseInt(opcion);
            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE HUELLAS*/
            Intent intent = new Intent(WebEnrolamiento.this, MainActivityEnrollMovil.class);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

            // intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, Integer.parseInt(tipoCaptura)); // *** Consultar *** opcionManos
            extra.putInt(ACT_HUELLAS_IMG, 1); //Dejar archivo de huellas
            extra.putInt(ACT_HUELLAS_NUM, Integer.parseInt(identificador)); //numeroFolio
            intent.putExtras(extra);
            //startActivityForResult(intent, LLAMAR_LECTOR_HUELLAS);
            checkIntentAndStart(intent, 100, NAME_LECTOR_HUELLAS);
        }

        @JavascriptInterface
        public void llamaComponenteHuellasV(int tipoCaptura, int identificador, int band, int opcion) {

            /* DIRECTORIOS DE COMPLEMENTOS */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, identificador+"");
            //iFolioSolicitud = identificador;//1001234;identificador
            iFolioEnrolamiento = identificador;
            opcionManos = tipoCaptura;

            /* Opcion de Huellas */
            opcionHuellas=opcion;
            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE HUELLAS*/
            Intent intent = new Intent(WebEnrolamiento.this, MainActivityEnrollMovil.class);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

            //intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, tipoCaptura); // *** Consultar *** opcionManos
            extra.putInt(ACT_HUELLAS_IMG, band); //Dejar archivo de huellas
            extra.putInt(ACT_HUELLAS_NUM, identificador); //numeroFolio
            intent.putExtras(extra);
            //startActivityForResult(intent, LLAMAR_LECTOR_HUELLASV);
            checkIntentAndStart(intent, 100, NAME_LECTOR_HUELLAS);
        }

        @JavascriptInterface
        public void llamaComponenteFirmaDigital(String servidor, String folioAfi, String folioSer, String tipoFirma) {

            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryFormatoEnrolamiento= new File(DIR_ACTUAL + "/" + folioAfi , "FormatoEnrolamiento");

            if(!directoryFormatoEnrolamiento.exists()) {
                if (directoryFormatoEnrolamiento.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            opcionFirmas = tipoFirma;

            if(tipoFirma=="0"){ tipoFirma="FTRAB";  }
            if(tipoFirma=="1"){ tipoFirma="FPROM";  }
            if(tipoFirma=="2"){ tipoFirma="NTRAB";  }


            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_FIRM_PKG,ACT_FIRM_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_FIRMA_FOAFI, Integer.parseInt(folioAfi));
            extra.putInt(ACT_FIRMA_FOSER, Integer.parseInt(folioSer));
            extra.putString(ACT_FIRMA_TIPOF, tipoFirma);
            extra.putString(ACT_FIRMA_PERSONAF, opcionFirmas);
            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_FIRMA_ENROLA);
        }

        @JavascriptInterface
        public void llamaComponenteDigitalizador(String servidor, int folioAfi, int numEmpleado, int folioServ, int tipoProceso, int tipoOperacion) {

            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryDigitalizador= new File(DIR_ACTUAL + "/" + folioAfi , "Digitalizador");

            if(!directoryDigitalizador.exists()) {
                if (directoryDigitalizador.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE FIRMA*/

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_DIGI_PKG,ACT_DIGI_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putString(ACT_DIGI_FOAFI, folioAfi +"");
            extra.putString(ACT_DIGI_NOEMP, numEmpleado +"");
            extra.putString(ACT_DIGI_TIPO, "2");
            extra.putString(TAG_RESP_FOLIO, folioServ +"");
            extra.putInt(ACT_TIPO_AFI, tipoProceso);

            /*if(tipoOperacion == 26 || tipoOperacion == 27 || tipoOperacion == 33){
                extra.putString(ACT_DIGI_TIPO, "1");
                extra.putString(ACT_DIGI_TABLA, "3");
            }else{
                extra.putString(ACT_DIGI_TIPO, "2");
                extra.putString(ACT_DIGI_TABLA, "8");
                extra.putString(ACT_DIGI_FOSER, folioServ + "");
            }*/

            extra.putString(ACT_DIGI_TIPO, "1");
            extra.putString(ACT_DIGI_TABLA, "3");

            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_DIGITALIZADOR);
        }

        @JavascriptInterface
        public void terminarEnrolamiento(String estatusproceso) {

            if(Integer.parseInt(estatusproceso) == 1){
                setResult(Activity.RESULT_OK);
            }else{
                setResult(Activity.RESULT_CANCELED);
            }

            finishAndRemoveTask();

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override //SE CANCELA CUALQUIER ACCIÓN CON EL BOTÓN DE REGRESAR.
    public void onBackPressed() {}

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LLAMAR_FIRMA_ENROLA) {

            //Toast.makeText(this, "LLAMAR_FIRMA_ENROLA", Toast.LENGTH_LONG).show();
            if(resultCode == Activity.RESULT_OK){

                if(data.hasExtra(ACT_FIRMA_NOMFIR)){
                    Bundle firma = data.getExtras();
                    String nameFirma = "";
                    nameFirma = firma.getString(ACT_FIRMA_NOMFIR);

                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteFirma('1', '" + opcionFirmas + "')");

                    // RUTA DE LAS IMAGENES DE LA FIRMA "/sysx/progs/gsoap/wsafiliacionmovil/imagenes"

                }
                else{
                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                    Toast.makeText(this,  R.string.msn_can_firma, Toast.LENGTH_LONG).show();
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                mWebViewEnrolamiento.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();

            }
        }else if(requestCode == LLAMAR_DIGITALIZADOR) {

            //Toast.makeText(this, "LLAMAR_FIRMA_ENROLA", Toast.LENGTH_LONG).show();
            if(resultCode == Activity.RESULT_OK){
                String estadoProceso="1";

                mWebViewEnrolamiento.loadUrl("javascript:getComponenteDigitalizar('"+ estadoProceso +"')");
                // RUTA DE LAS IMAGENES DIGITALIZADAS "/sysx/smbx"

            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                String estadoProceso="0";
                mWebViewEnrolamiento.loadUrl("javascript:getComponenteDigitalizar('"+ estadoProceso +"')");

                Toast.makeText(this,  R.string.msn_can_capdigit, Toast.LENGTH_LONG).show();

            }
        }else{
            switch (requestCode){
                case LLAMAR_LECTOR_HUELLAS:
                    if(resultCode == RESULT_OK) {

                        fromHuellasToTemplate(data); //LLAMA AL SERVICIO PARA GENERAR LOS TEMPLATES DE LAS HUELLAS LEIDAS

                    }
                    else if(resultCode == RESULT_CANCELED){ //SI OCURRE UN ERROR EN LA TOMA DE HUELLAS
                        showToast(this,
                                getResources().getString(R.string.msn_can_huellas));
                    }
                    break;
                case LLAMAR_LECTOR_HUELLASV:
                    if(resultCode == RESULT_OK) {

                         mWebViewEnrolamiento.loadUrl("javascript:terminarEnrolamiento('1')");

                    }
                    else if(resultCode == RESULT_CANCELED){ //SI OCURRE UN ERROR EN LA TOMA DE HUELLAS

                        mWebViewEnrolamiento.loadUrl("javascript:terminarEnrolamiento('0')");

                    }
                    break;
                case LLAMAR_DIALOGO_EXCEP:

                    if(resultCode == RESULT_OK) {
                        Bundle args = data.getBundleExtra("BUNDLE");
                        dataFigers = (ArrayList<DataFingerModel>) args.getSerializable("ARRAYLIST");

                        if(dataFigers != null)
                            for(DataFingerModel dedo : dataFigers){
                                arrFingers.setExcepcion(dedo.getFingerId()-1, dedo.getFingerException());
                            }

                        soapGuardarDatosReenrol();
                    }
                    else if(resultCode == RESULT_CANCELED){
                        showToast(this,
                                getResources().getString(R.string.msn_can_excep));
                    }

                    break;
                case LLAMAR_FORMATO_ENROL:

                    if(resultCode == Activity.RESULT_OK){

                        mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('1', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')");

                    }
                    else if (resultCode == Activity.RESULT_CANCELED) {
                        mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')");
                    }

                    break;

            }
        }
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {

        MessageDialog aviso;

        if(xmlMen.equals("")){

            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_ser_res));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) ->
                            mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

            showMessage(this, aviso);
            return;
        }



        HashMap<String, String> values = getValuesXML(xmlMen);

        if(values == null){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_ser_xml));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) ->
                            mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

            showMessage(this, aviso);
            return;
        }
        int iEstatus=0;

        if(values.get("Estatus") != "" && values.get("Estatus") != null) {
             iEstatus = Integer.parseInt(values.get("Estatus"));
        }else{
            iEstatus = 200;
        }

        String esMensaje = values.get("EsMensaje");

        if(iEstatus != 200 && iEstatus != 201){
            aviso = new MessageDialog();

            if(esMensaje.equals(" ")) {
                aviso = new MessageDialog();
                aviso.setMensaje(getResources().getString(R.string.msn_err_ser_res));
                aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                        (dialogInterface, i) ->
                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));


            }else {
                aviso = new MessageDialog();
                aviso.setMensaje(esMensaje);
                aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                        (dialogInterface, i) ->
                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

            }

            showMessage(this, aviso);

            return;
        }

        //SE VALIDA DE CUAL LLAMADO VUELVE EL WEBSERVICE
        switch (id){
            case SOAP_OBTENER_IP:
                ip = values.get("ip");
                writeLog("[soapObtenerIp] soapObtenerIp ip" + ip);
                soapComparacionTemplate();
                break;
            case SOAP_GUARDAR_EMP_AUTORIZA:
                /*soapGenerarFormatoEnrol();*/
                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('1', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')");
                break;

            case SOAP_COMPARACION_TEMPLATE:
                writeLog("[SOAP_COMPARACION_TEMPLATE] SOAP_COMPARACION_TEMPLATE");
                int IdDedo = 0;
                String Mensaje = "";
                IdDedo = Integer.parseInt(values.get("lEstado"));
                Mensaje = values.get("cDescripcion");

                writeLog("respuestaCompara "+IdDedo+" "+Mensaje);
                try {
                    if( IdDedo > 0){
                        numdedo = IdDedo+"";
                        contMatch = 0;
                        compararOnceavaHuella(fingerEmpleado);
                    }else{
                        contMatch = -1;
                        compararOnceavaHuella(fingerEmpleado);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case SOAP_GUARDAR_HUELLAS:

                int iResultado = Integer.parseInt(values.get("ResultadoActualizar"));

                if(iResultado == 4 || iResultado == 3) {
                    //CAMBIO DE FLUJO PARA CAPTURAR 11va HUELLA (LA DEL PROMOTOR)
                    opcionManos = 4;

                    aviso = new MessageDialog();
                    aviso.setMensaje(getResources().getString(R.string.msn_avi_huella));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) ->
                                    llamaComponenteHuellas());
                    showMessage(this, aviso);
                }
                else {
                    aviso = new MessageDialog();
                    aviso.setMensaje(getResources().getString(R.string.msn_err_enr_huellas));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) ->
                                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));
                    showMessage(this, aviso);
                }

                break;
            case SOAP_FOLIO_ENROLAMIENTO:

                iFolioEnrolamiento = Integer.parseInt(values.get("ResultadoActualizar"));
                //iFolioEnrolamiento = iFolioEnrolamiento - 1;
                writeLog("Folio Enrolamiento " + iFolioEnrolamiento);
                if(iFolioEnrolamiento > 0){
                    flagReenrol = true;

                    if(dataFigers.size() > 0) {
                        dedosExcepcion();
                    }else {
                        soapGuardarDatosReenrol();
                    }
                }
                else{
                    aviso = new MessageDialog();
                    aviso.setMensaje(getResources().getString(R.string.msn_err_enr_folio));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) ->
                                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));
                    showMessage(this, aviso);
                }

                break;
            case SOAP_GEN_FORMATO_REENROL:

                String nameFor = values.get("cNomPdf");
                String cadena64 = values.get("sImagen");
                byte[] bytes64  = cadena64.getBytes();

                DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
                File dirFor = new File(DIR_ACTUAL, DIR_FORMATO);
                if(!dirFor.exists())
                    if(!dirFor.mkdir())
                        writeLog("Ocurrió un error al crear la carpeta del Formato de Enrolamiento  ");

                File dwldsPath = new File(dirFor, nameFor);
                byte[] pdfAsBytes = Base64.decode(bytes64, 0);

                writeFile(dwldsPath, pdfAsBytes, false);

                try {

                    Intent i = new Intent(getApplicationContext(), FormatoEnrolActivity.class);
                    Bundle extra = new Bundle();
                    extra.putString(CONFIG_KEY1, ipAddress);
                    extra.putString("ipOffline", ipOffline);
                    extra.putString(ACT_FORMATO_PDF, nameFor);
                    extra.putInt(ACT_FORMATO_FOAFI, iFolioSolicitud);
                    extra.putInt(ACT_FORMATO_FOSER, iFolioServicio);
                    extra.putString(ACT_FORMATO_FIRMA, "FTRAB");
                    i.putExtras(extra);
                    startActivityForResult(i, LLAMAR_FORMATO_ENROL);

                }catch (Exception e){
                    writeLog("Exception" + e.getStackTrace());
                }

                break;

            default:
                break;
        }

    }

    public void dedosExcepcion(){
        Intent i = new Intent(WebEnrolamiento.this, RazonExcepcionesActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST", dataFigers);
        i.putExtra("BUNDLE",args);
        startActivityForResult(i, LLAMAR_DIALOGO_EXCEP);
    }


    private void llamaComponenteHuellas(){
        /*LLAMADO COMPONENTE DE HUELLAS*/
        Intent intent = new Intent(WebEnrolamiento.this, MainActivityEnrollMovil.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

        // intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

        Bundle extra = new Bundle();
        extra.putInt(ACT_HUELLAS_OPC, opcionManos); // *** Consultar *** opcionManos
        extra.putInt(ACT_HUELLAS_IMG, 1); //Dejar archivo de huellas
        extra.putInt(ACT_HUELLAS_NUM, iEmpleado);
        intent.putExtras(extra);
        startActivityForResult(intent, 100);
        //checkIntentAndStart(intent, LLAMAR_LECTOR_HUELLAS, NAME_LECTOR_HUELLAS);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void fromHuellasToTemplate(Intent data){
        MessageDialog aviso = new MessageDialog();
        MessageDialog errorMsn = new MessageDialog();
        int     OUT_1 = -1;
        String  OUT_2 = "";//, OUT_3 = "", OUT_4 = "";
        String ruta;
        Bundle resApp = data.getExtras();
        if(resApp != null) {
            OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);

            if(opcionManos == 4){
                ruta = DIR_EXTERNA + "sys/mem/" + iEmpleado + "/JsonEnviado_" + iEmpleado + ".txt";
                writeLog(" ruta " + ruta);
            }else {
                ruta = DIR_EXTERNA + "sys/mem/" + iFolioSolicitud + "/JsonEnviado_" + iFolioSolicitud + ".txt";
                writeLog(" ruta " + ruta);
            }
            try {
                OUT_2 = readFile(ruta).trim();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //OUT_2 = resApp.getString (ACT_HUELLAS_OT2);
        }

        switch (OUT_1) {
            case 1: //EXITO EN LA TOMA DE HUELLAS

                JSONObject param;
                String url;

                if(opcionManos == 4 || opcionManos == 7){
                    param = prepararJSONEmpleado(OUT_2);
                    url = REST_EMPL + REST_IMGE;
                }else{
                    param = prepararJSONFingers(OUT_2);
                    url = REST_CLIE + REST_IMGE;
                }


                if(param == null){
                    /*aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_excep));
                    showMessage(this, aviso);*/
                    aviso = new MessageDialog();
                    aviso.setMensaje(getResources().getString(R.string.msn_err_enr_excep));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) ->
                                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento + "')"));
                    showMessage(this, aviso);
                }
                else{

                     //GENERAR TEMPLATES
                    /*writeLog("========================= ENROLAMIENTO =========================");
                    writeLog("[fromHuellasToTemplate] url" + url);
                    writeLog("[fromHuellasToTemplate] param" + param);*/
                     JsonObjectRequest jsObjRequest = new JsonObjectRequest
                            (Request.Method.POST, url, param, response -> {
                                //writeLog("" + response);
                                if (opcionManos != 4)
                                    guardarTemplatesCliente(response);
                                else
                                    getOnceavaHuella(response);
                            }, error -> {
                                writeLog("[fromHuellasToTemplate] " + error);
                                /*MessageDialog aviso = new MessageDialog();
                                aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_templates));
                                showMessage(this, aviso);*/
                               //MessageDialog aviso = new MessageDialog();
                                MessageDialog avisoError = new MessageDialog();
                                avisoError = new MessageDialog();
                                avisoError.setMensaje(getResources().getString(R.string.msn_err_enr_templates));
                                avisoError.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                                        (dialogInterface, i) ->
                                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento + "')"));
                                showMessage(this, avisoError);

                            });

                    queue.add(jsObjRequest);
                }

                break;

            case 99: //PROCESO CANCELADO
                aviso = new MessageDialog();
                aviso.setMensaje(getResources().getString(R.string.msn_can_huellas));
                aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                        (dialogInterface, i) ->
                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')"));
                showMessage(this, aviso);
                break;
            case 0:
                aviso = new MessageDialog();
                aviso.setMensaje(getResources().getString(R.string.msn_err_enr_catura));
                aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                        (dialogInterface, i) ->
                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')"));
                showMessage(this, aviso);
                break;
            default: //ERROR INESPERADO
                aviso = new MessageDialog();
                aviso.setMensaje(getResources().getString(R.string.msn_err_enr_otro));
                aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                        (dialogInterface, i) ->
                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')"));
                showMessage(this, aviso);
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public JSONObject prepararJSONEmpleado(String OUT_2){
        writeLog("prepararJSONEmpleado");
        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();
        String excepcion;
        String Maker, Model, Serial, Firmware, IpOrigen;

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");
            Maker = respLector.getString("Maker");
            Model = respLector.getString("Model");
            Serial = respLector.getString("Serial");
            Firmware = respLector.getString("Firmware");
            IpOrigen = getIp();

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                excepcion = finger.getString("ExceptionId");

                if(excepcion.equals("1")) {
                    finger.put("Nfiq", "5");
                }

            }

            File jsonResp = new File(DIR_ACTUAL, "jsonrespuestaempleado" + todayDate + ".json");
            writeFile(jsonResp, respLector.toString().getBytes(), false);

            dispositivo = Serial;
            finger = arrayEx.getJSONObject(0);
            imagen = finger.getString("B64FingerImage");
            nist = finger.getString("Nfiq");

            param.put("NumberEmp", JSONObject.numberToString(iEmpleado));
            param.put("EmpAutoriza", JSONObject.numberToString(iEmpleado));
            param.put("Fingerprints", arrayEx);
            param.put("FingersException", respLector.get("FingersException"));
            param.put("Maker",Maker);
            param.put("Model",Model);
            param.put("Serial",Serial);
            param.put("Firmware",Firmware);
            param.put("IpOrigen",IpOrigen);

        } catch (JSONException e) {
            //cargando(pbCargando, false); //QUITAR PANTALLA DE CARGA
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
            MensajeLog = "Error en JSON de prepararJSONempleado | "+OUT_2;
            //soapGuardarLogs();
        }

        return param;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public String getIp(){
        int ip;

        UsbManager m = (UsbManager)getApplicationContext().getSystemService(USB_SERVICE);
        HashMap<String, UsbDevice> devices = m.getDeviceList();
        Collection<UsbDevice> ite = devices.values();
        UsbDevice[] usbs = ite.toArray(new UsbDevice[]{});

        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        ip = wifiInfo.getIpAddress();

        @SuppressWarnings("deprecation")
        String IpOrigen = Formatter.formatIpAddress(ip);


        return IpOrigen;
    }

    public JSONObject prepararJSONFingers(String OUT_2){

        int id;
        int nfiq;
        int contEx = 0;
        String excepcion;

        //writeLog(OUT_2);

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();

        arrFingers = new FingerArray();
        dataFigers = new ArrayList<>();
        String[] nom_dedos = getResources().getStringArray(R.array.enrol_dedos);

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");

            //writeLog("================================================== ");
            //writeLog("[prepararJSONFingers] respLector " + respLector);

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                id          = Integer.parseInt(finger.getString("FingerId"));
                nfiq        = Integer.parseInt(finger.getString("Nfiq"));
                excepcion   = finger.getString("ExceptionId");

                if(nfiq == 5){
                    excepcion = "1";
                }

                if(excepcion.equals("1")) {
                    contEx++;

                    if(opcionManos == HUELLAS_IZQ_FALTA && id > 5){
                        excepcion = "0";
                        dataFigers.add(new DataFingerModel(id, nom_dedos[id - 1], "0"));
                    }else if(opcionManos == HUELLAS_DER_FALTA && id < 6){
                        excepcion = "0";
                        dataFigers.add(new DataFingerModel(id, nom_dedos[id-1], "0"));
                    }else{
                        excepcion = "0";
                        dataFigers.add(new DataFingerModel(id, nom_dedos[id-1], "0"));
                    }

                    JSONObject newFinger = new JSONObject(finger.toString());
                    newFinger.put("Nfiq", "5");
                    arrayEx.put(i,newFinger);
                }
                addFinger(finger);
            }

            //writeLog("================================================== ");
            //writeLog("[prepararJSONFingers] contEx " + contEx);

            if(contEx > 7){

                param = null;

            }else{
                param.put("Cliente", JSONObject.numberToString(iFolioSolicitud));
                param.put("Empleado", JSONObject.numberToString(iEmpleado));
                param.put("Fingerprints", arrayEx);
                param.put("Maker", respLector.get("Maker"));
                param.put("Model", respLector.get("Model"));
                param.put("Serial", respLector.get("Serial"));
                param.put("Firmware", respLector.get("Firmware"));
                param.put("FingersException", respLector.get("FingersException"));
                param.put("ExceptionQuality", respLector.get("ExceptionQuality"));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public void guardarTemplatesCliente(JSONObject response) {
        try {

            int apiEstatus = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            int apiResultado = response.getInt(REST_RP_RES);

            if (apiEstatus == 201 && apiResultado == 0) {

                JSONArray arrTemplates = response.getJSONArray("FingerTemplate");
                JSONObject finger;

                String template;

                for(int k=0; k<arrFingers.getSize(); k++){
                    finger = arrTemplates.getJSONObject(k);
                    template    = finger.getString("Template");

                    arrFingers.setTemplate(k, template);
                }

                //GUARDA TEMPLATE DEL EMPLEADO
                DIR_ACTUAL = DIR_EXTERNA + DIR_AFILIACION;
                File jsonResp = new File(DIR_ACTUAL, "tempCliente" + todayDate + ".json");
                writeFile(jsonResp, response.toString().getBytes(), false);

                soapFolioEnrolamiento(); //SE GENERA EL FOLIO ENROLAMIENTO
            } else {
                showToast(this,
                        getResources().getString(R.string.msn_err_enr_enrolar));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_ser_con));
        }
    }

    public void getOnceavaHuella(JSONObject response){
        try {

            int apiEstatus = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            int apiResultado = response.getInt(REST_RP_RES);

            if (apiEstatus == 201 && apiResultado == 0) {

                writeLog("[getOnceavaHuella] " + apiMensaje);

                //OBTENER EL TEMPLATE DEL EMPLEADO
                fingerEmpleado = response.getJSONArray("FingerTemplate").getJSONObject(0);
                fingerEmpleado.put("FingerId", 11);
                template = response.getJSONArray("FingerTemplate").getJSONObject(0).get("Template").toString();

                writeLog("[getOnceavaHuella] fingerEmpleado Finalizado");

                //COMPARA LAS HUELLAS ENROLADAS CON LA HUELLA TOMADA DEL PROMOTOR
                //compararOnceavaHuella(fingerEmpleado);

                soapObtenerIp();

            } else {
                writeLog("[getOnceavaHuella] Error en respuesta");
               // cargando(pbCargando, false); //QUITAR PANTALLA DE CARGA
                showToast(this,
                        getResources().getString(R.string.msn_err_enr_catura));
                MensajeLog = "[getOncevaHuella]"+response.toString();
                //soapGuardarLogs();
            }

        } catch (JSONException e) {
            writeLog("[getOnceavaHuella] Error en JSON");
            //cargando(pbCargando, false); //QUITAR PANTALLA DE CARGA
            showToast(this,
                    getResources().getString(R.string.msn_err_ser_con));
            MensajeLog = "Error al obtener el JSON response getOnceavaHuella | "+response.toString();
            //soapGuardarLogs();
        }
    }


    public void addFinger(JSONObject finger) throws JSONException{
        int id;
        int nfiq;
        String base64;
        String excepcion;

        id          = Integer.parseInt(finger.getString("FingerId"));
        nfiq        = Integer.parseInt(finger.getString("Nfiq"));
        base64      = replaceBase64(finger.getString("B64FingerImage"));
        excepcion   = finger.getString("ExceptionId");

        //CREAR EL OBJETO PARA EL DEDO
        arrFingers.newFinger(id, nfiq, base64, excepcion);
    }

    public String replaceBase64(String base64){
        base64 = base64.replace('_','/');

        return base64.replace('-','+');
    }

    public void soapFolioEnrolamiento(){

        writeLog("txtCurpCliente " + txtCurpCliente);
        writeLog("iEmpleado " + iEmpleado);
        writeLog("iTipoSolicitud " + iTipoSolicitud);
        writeLog("iFolioSolicitud " + iFolioSolicitud);

        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarReenrolamiento");
        sSoap.setParams("sCurp", txtCurpCliente);
        sSoap.setParams("iEmpleado", iEmpleado + "");
        sSoap.setParams("iTipoSol", iTipoSolicitud + "");
        sSoap.setParams("iFolioAfiliacion", iFolioSolicitud + "");

        writeLog("Obtener Folio Enrolamiento [FolioAfiliacion] = [" + iFolioSolicitud + "]");

        CallWebService callWS = new CallWebService(this, SOAP_FOLIO_ENROLAMIENTO);
        callWS.execute(sSoap.getSoapString());

    }

    public void soapGuardarDatosReenrol(){

        apiActualizarFolioEnrolamiento(iFolioSolicitud+"",iFolioEnrolamiento+"");

        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarDatosReenrolamiento");
        sSoap.setParams("iFolioEnrolamiento", iFolioEnrolamiento + "");
        sSoap.setParams("cDispositivo", "1815-00062");
        sSoap.setParams("iFap", "50");
        sSoap.setParams("FingersData", arrFingers.getFingersXml());

        CallWebService callWS = new CallWebService(this, SOAP_GUARDAR_HUELLAS);
        callWS.execute(sSoap.getSoapString());
    }

    public void eliminarArchivos(){
        DIR_ACTUAL = DIR_EXTERNA + DIR_AFILIACION;
        File tempClien = new File(DIR_ACTUAL + "tempCliente" + todayDate + ".json");
        if(tempClien.delete())
            writeLog("[eliminarArchivos] Archivo Eliminado: tempCliente" + todayDate + ".json");
        Log.e("Archivo", "Eliminado");
    }

    /* public void compararHuellas(JSONObject param){

        String url = REST_CLIE + REST_TENF;

        //GENERAR TEMPLATES
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, param, this::comprobarOnceavaHuella, error -> {
                    writeLog("[compararHuellas] " + error);
                    MessageDialog aviso = new MessageDialog();
                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_compara));
                    showMessage(this, aviso);
                });

        queue.add(jsObjRequest);
    } */
    /*
    public void comprobarOnceavaHuella(JSONObject response){
        MessageDialog aviso = new MessageDialog();
        try{
            int apiEstatus    = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            //int apiResultado  = response.getInt(REST_RP_RES);
            if(apiEstatus == 200 || apiEstatus == 201){

                if(apiMensaje.equals("Match")){

                    aviso = new MessageDialog();
                    aviso.setMensaje(getResources().getString(R.string.msn_err_enr_match));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) ->
                                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')"));
                    showMessage(this, aviso);
                }else{
                    soapGenerarFormatoEnrol();

                }

            }else{

                aviso = new MessageDialog();
                aviso.setMensaje(getResources().getString(R.string.msn_err_ser_con));
                aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                        (dialogInterface, i) ->
                                mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')"));
                showMessage(this, aviso);
            }

        }catch(JSONException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_ser_res));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) ->
                            mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas +"', '" + iFolioEnrolamiento +"')"));
            showMessage(this, aviso);
        }
    }*/

    public void soapGenerarFormatoEnrol() {
        //iFolioEnrolamiento = 4938;
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GenerarFormatoReenrol");
        sSoap.setParams("iFolioEnrol", iFolioEnrolamiento + "");

        writeLog("Generar Formato [Folio Enrolamiento] = [" + iFolioEnrolamiento + "]");

        CallWebService callWS = new CallWebService(this, SOAP_GEN_FORMATO_REENROL);
        callWS.execute(sSoap.getSoapString());
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    private static String readFile(String path) throws IOException {
        FileInputStream stream = new FileInputStream(new File(path));
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /*Instead of using default, pass in a decoder.*/
            return Charset.defaultCharset().decode(bb).toString();
        }
        finally {
            stream.close();
        }
    }

    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {
            }
        }
    }

    private void checkIntentAndStart(Intent intent, int idActivity, String application){
        PackageManager manager = this.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            startActivityForResult(intent, idActivity);
        } else {
            MessageDialog aviso = new MessageDialog();
            aviso.setTitle("Falta de aplicación");
            aviso.setMensaje("Es necesario instalar la aplicación: <b>" + application + "<b>");
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) ->
                            mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

            showMessage(this, aviso);
        }
    }


    private void apiActualizarFolioEnrolamiento(String sFolio, String sFolioEnrolamiento) {

        /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
        ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);
        ipServidor = ipAddress.replace(IP_SERV_PROD_WS,IP_SERV_PROD);*/
        apiInterface = APIClient.getClient(ipOffline).create(APIInterface.class);
        String var = infoFolio.createFolioEnrolameinto(sFolio, sFolioEnrolamiento);

        try {
            Call<FolioEnrolamiento> call = apiInterface.cambiarFolioEnrolamiento(var);
            call.enqueue(new Callback<FolioEnrolamiento>() {
                @Override
                public void onResponse(Call<FolioEnrolamiento> call, Response<FolioEnrolamiento> response) {

                    FolioEnrolamiento resource = response.body();
                    Integer estatus = resource.estatus;
                    String descripcion = resource.descripcion;
                    String respuesta = resource.respuesta;

                }

                @Override
                public void onFailure(Call<FolioEnrolamiento> call, Throwable t) {
                    call.cancel();
                }
            });

        }catch (Exception e){
            writeLog("Exception e.toString() " + e.toString());
        }


    }

    private void apiActualizarConexion(String sFolio) {

        if (Utilerias.existeConexionWiFi(this, 0)) {
            iTipoConexion=2;
        }else{
            iTipoConexion=3;
        }
        apiInterface = APIClient.getClient(ipOffline).create(APIInterface.class);
        String var = infoConexion.createConexion(sFolio, iTipoConexion+"");

        try {
            Call<TipoConexion> call = apiInterface.cambiarConexion(var);
            call.enqueue(new Callback<TipoConexion>() {
                @Override
                public void onResponse(Call<TipoConexion> call, Response<TipoConexion> response) {

                    TipoConexion resource = response.body();
                    Integer estatus = resource.estatus;
                    String descripcion = resource.descripcion;
                    String respuesta = resource.respuesta;

                }

                @Override
                public void onFailure(Call<TipoConexion> call, Throwable t) {
                    call.cancel();
                }
            });

        }catch (Exception e){
            writeLog("Exception e.toString() " + e.toString());
        }


    }

    private void soapObtenerIp(){

        writeLog("[soapObtenerIp] soapObtenerIp INICIO");

        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "ObtenerDatoIp");
        sSoap.setParams("apikey", apikey);
        sSoap.setParams("identificador","1");
        sSoap.setParams("iTabla", "2");

        flagSoap = false;

        CallWebService callWS = new CallWebService(this, SOAP_OBTENER_IP);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());

        writeLog("[soapObtenerIp] soapObtenerIp FIN");

    }

    private void soapComparacionTemplate(){

        writeLog("[soapComparacionTemplate] soapComparacionTemplate INICIO");

        //OBTENER TEMPLATE DEL EMPLEADO EN BASE DE DATOS
        new CallWebService(ipwshuellashc);
        SoapString sSoap = new SoapString("wsHuellasHC",
                "AforeComparacionTemplate");
        sSoap.setParams("lNumero", iEmpleado+"");
        sSoap.setParams("cTemplate",template );
        sSoap.setParams("iSizeTemplate",template.length()+"");
        sSoap.setParams("cIpAfore",ip);
        sSoap.setParams("iTipoPersona","1");

        flagSoap = false;

        CallWebService callWS = new CallWebService(this, SOAP_COMPARACION_TEMPLATE);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());

        writeLog("[soapComparacionTemplate] soapComparacionTemplate FIN");

    }
    public void compararOnceavaHuella(JSONObject onceava) throws JSONException, IOException {

        writeLog("[compararOceavaHuella] Inicia proceso de comparación");

        contCall = 0;
        flagError = false;
        flagProm = false;

        /* *********************COMPARAR TEMPLATE TOMADO CON EL DEL EMPLEADO********************** */
        /* *************************************************************************************** */
        //OBTIENE LOS TEMPLATES DEL EMPLEADO QUE SE OBTUVIERON DE BD
        String stringEmpleado = readFile(DIR_EXTERNA + DIR_REENROL + "compareTempEmp.json");
        JSONObject temEmp = new JSONObject(stringEmpleado);

        JSONArray comEmp = temEmp.getJSONArray("FingerTemplate");
        comEmp.put(10, onceava);

        /* *************************************************************************************** */

        //OBTIENE LOS TEMPLATES DEL CLIENTE QUE PREVIAMENTE SE HABÍAN TOMADO
        String stringClientes = readFile(DIR_ACTUAL + "tempCliente" + todayDate + ".json");
        JSONObject templatesCliente = new JSONObject(stringClientes);

        //JSON QUE SE ENVIARÁ AL SERVICIO QUE COMPARA LOS TEMPLATES
        JSONObject tempCompara = new JSONObject();
        tempCompara.put("Cliente", iFolioSolicitud + "");

        //ARRAY QUE CONTENDRÁ LOS 11 TEMPLATES A COMPARAR
        JSONArray allFingers = templatesCliente.getJSONArray("FingerTemplate");

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        String url;

        url = REST_CLIE + REST_TENF;

        contTotal = comEmp.length();

        for(int i=0; i < comEmp.length() - 1;i++){
            JSONObject finger = new JSONObject(comEmp.getJSONObject(i).toString());
            finger.put("FingerId", "11");
            allFingers.put(10, finger);

            tempCompara.put("FingerTemplate", allFingers);
            comparaEmpleadoDedos(tempCompara, false, url);
        }

        //SE AGREGA EL 11vo TEMPLATE
        allFingers.put(10, onceava);

        //SE AGREGA ARRAY AL OBJETO JSON
        tempCompara.put("FingerTemplate", allFingers);
        comparaEmpleadoDedos(tempCompara, false, url);
    }

    public void comparaEmpleadoDedos(JSONObject templates, boolean flag, String url) throws JSONException{
        JSONObject empleado = new JSONObject(templates.toString());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, empleado, response ->
                        respuestasComparaciones(response, flag),
                        error -> {
                            //cargando(pbCargando, false); //QUITAR PANTALLA DE CARGA
                            flagError = true;
                            writeLog("[comparaEmpleadoEmpleado] " + error.networkResponse);

                            MensajeLog = "URL: "+url+" | "+" Templates Empleado: "+templates.toString();
                            //soapGuardarLogs();

                            MensajeLog = error.toString();
                            //soapGuardarLogs();
                        });

        queue.add(jsObjRequest);
    }
    public void respuestasComparaciones(JSONObject response, boolean flag){
        MessageDialog aviso = new MessageDialog();
        try{
            int apiEstatus    = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);

            if(apiEstatus == 200 || apiEstatus == 201){
                contCall++;
                if(apiMensaje.equals("Match")){
                    if(flag)
                        contMatch++;
                    else
                        writeLog("[respuestasComparaciones] Huella de cliente coincide con promotor");
                }else{
                    if(!flag)
                        contMatch++;
                    else{
                        flagProm = true;
                        writeLog("[respuestasComparaciones] No coincide huella promotor");
                    }
                }
            }else{
                writeLog("[respuestasComparaciones] " + R.string.msn_err_ser_con);
                flagError = true;
                MensajeLog = "Error en respuestasComparaciones | "+response.toString();
                //soapGuardarLogs();
            }

        }catch(JSONException e){
            writeLog("[respuestasComparaciones] " + R.string.msn_err_ser_res);
            flagError = true;
            MensajeLog = "Error al obtener el JSON reponse respuestasComparaciones| "+flag+" "+response.toString();
            //soapGuardarLogs();
        }

        if(contCall == contTotal) {
           // cargando(pbCargando, false);
            writeLog("[respuestasComparaciones] Total de llamadas");
            writeLog("[respuestasComparaciones] contMatch: "+contMatch);
            if (contMatch == contTotal) {
                eliminarArchivos();
                soapGuardarTrabajadorAutoriza();
            } else {
                writeLog("[respuestasComparaciones] contMatch: "+contMatch+" contTotal: "+contTotal);
                if(flagProm){
                    if(contPromHuella < 3) {
                        writeLog("[respuestasComparaciones] contPromHuella: "+contPromHuella);
                        tomaHuellaEmpleado(getResources().getString(R.string.msn_err_enr_match_prom));
                    }else {
                        eliminarArchivos();
                        aviso.setMensaje(getResources().getString(R.string.msn_err_enr_match_promfinal));
                        aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                                (dialogInterface, i) ->
                                        mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

                        showMessage(this, aviso);
                    }
                }
                else {
                    aviso.setMensaje(getResources().getString(R.string.msn_err_enr_match));
                    aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                            (dialogInterface, i) ->
                                    mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

                    showMessage(this, aviso);
                }
            }
        }else if(flagError){
            writeLog("[respuestasComparaciones] Error en alguna llamada");
           // cargando(pbCargando, false);
            aviso.setMensaje(getResources().getString(R.string.msn_err_ser_con));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) ->
                            mWebViewEnrolamiento.loadUrl("javascript:getComponenteHuellasCapturadas('0', '" + opcionHuellas + "', '" + iFolioEnrolamiento + "')"));

            showMessage(this, aviso);

        }
    }
    //**********************************************************************************//
    //**********************************************************************************//
    /*private void soapGuardarLogs(){
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog);

        flagSoap = true;

        CallWebService callWS = new CallWebService(null, 0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }*/

    public void soapGuardarTrabajadorAutoriza(){
        //cargando(pbCargando, true);

        new CallWebService(ipAddress);

        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarTrabajadorAutoriza");
        sSoap.setParams("numfuncionario", iEmpleado+"");
        sSoap.setParams("folioenrolamiento", iFolioEnrolamiento+"");
        sSoap.setParams("numdedo", numdedo);
        sSoap.setParams("nist", nist);
        sSoap.setParams("fap", fap);
        sSoap.setParams("imagen", imagen);
        sSoap.setParams("dispositivo", dispositivo);
        sSoap.setParams("identificador","1");

        flagSoap = true;

        writeLog("GuardarTrabajadorAutoriza - "+iEmpleado);
        writeLog("GuardarTrabajadorAutoriza - "+iFolioEnrolamiento+"");
        writeLog("GuardarTrabajadorAutoriza - "+numdedo);
        writeLog("GuardarTrabajadorAutoriza - "+nist);
        writeLog("GuardarTrabajadorAutoriza - "+fap);
        writeLog("GuardarTrabajadorAutoriza - "+dispositivo);
        CallWebService callWS = new CallWebService(this, SOAP_GUARDAR_EMP_AUTORIZA);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void tomaHuellaEmpleado(String msn){
        MessageDialog aviso = new MessageDialog();
        aviso.setMensaje(msn);
        aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                (dialogInterface, i) ->
                        llamaComponenteHuellas());
        showMessage(this, aviso);
    }


}

