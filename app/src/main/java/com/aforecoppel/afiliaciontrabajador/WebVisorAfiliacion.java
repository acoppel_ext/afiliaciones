package com.aforecoppel.afiliaciontrabajador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.*;

public class WebVisorAfiliacion extends AppCompatActivity implements AsyncTaskCompleteListener<String>{
    private WebView mWebView;
    private Button mButton;
    private EditText txtEmpleado;
    private int opcionManos; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACION
    private String DIR_ACTUAL;
    private int opcionHuellas;
    private String opcionFirmas;
    private int iEmpleado;
    private int iFolioSolicitud;
    private String todayDate;
    private String txtCurpCliente;
    private int iTipoSolicitud;
    private int iFolioEnrolamiento;
    private String opcionExcepcion="006";
    public static Context context;
    private String ipAddress, ipServidor, ipMovil;
    private String nameFirma = "";
    private int tipoFirmaD;
    private String ipOffline = "";
    private String ipwshuellashc = "";
    private FingerArray arrFingers;
    private ArrayList<DataFingerModel> dataFigers;

    Activity activity;
    private ProgressDialog progDailog;

    //VARIABLES DE LA ACTIVIDAD
    private RequestQueue queue;

    private String datosTemplate;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor_afiliacion);
        context = this;
        queue = Volley.newRequestQueue(this);

        NukeSSLCerts ObjectNukeSSL = new NukeSSLCerts();
        ObjectNukeSSL.nuke();

        //VALORES QUE LLEGAN DEL SUBMENÚ
        Intent intent = getIntent();
        getParameters(intent);

        if (!isOnline()) {

            Intent intentError = new Intent(this, errorConexion.class);
            this.startActivity(intentError);

        } else {

            Date sysDate = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
            todayDate = dateFormat.format(sysDate);

            mWebView = (WebView) findViewById(R.id.webview);
            WebSettings mWebSettings = mWebView.getSettings();

            mWebSettings.setJavaScriptEnabled(true);
            JavaScriptInterface jsInterface = new JavaScriptInterface();
            mWebView.addJavascriptInterface(jsInterface, "Android");

            /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
            ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);*/

            writeLog("========== ipOffline ==========" + ipOffline);

            mWebView.loadUrl( ipOffline + "/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=" + iEmpleado);

        }

    }

    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            ipOffline = intent.getStringExtra("ipOffline");
            iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);
            ipwshuellashc = intent.getStringExtra("ipwshuellashc");

        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }

    @SuppressLint("JavascriptInterface")
    public class JavaScriptInterface {

        @JavascriptInterface
        public void llamaComponenteHuellas(String tipoCaptura, String identificador, String band, String opcion, String empleado, String tiposolicitud, String curp) {

            /* Para generar el template */
            //iEmpleado = empleado;
            txtCurpCliente = curp;
            iTipoSolicitud = Integer.parseInt(tiposolicitud);

            /* DIRECTORIOS DE COMPLEMENTOS */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, identificador+"");
            iFolioSolicitud = Integer.parseInt(identificador);
            iFolioEnrolamiento = Integer.parseInt(identificador);
            opcionManos = Integer.parseInt(tipoCaptura);

            /* Opcion de Huellas */
            opcionHuellas=Integer.parseInt(opcion);
            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE HUELLAS*/
            Intent intent = new Intent(WebVisorAfiliacion.this, MainActivityEnrollMovil.class);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

            //intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, Integer.parseInt(tipoCaptura)); // *** Consultar *** opcionManos
            extra.putInt(ACT_HUELLAS_IMG, 1); //Dejar archivo de huellas
            extra.putInt(ACT_HUELLAS_NUM, Integer.parseInt(identificador)); //numeroFolio
            intent.putExtras(extra);
            startActivityForResult(intent, 100);
        }

        @JavascriptInterface
        public void llamaComponenteFirmaD(String folioAfi, String folioSer, String tipoFirma) {

            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            iFolioSolicitud = Integer.parseInt(folioAfi);
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryFormatoEnrolamiento= new File(DIR_ACTUAL + "/" + folioAfi , "FormatoEnrolamiento");

            if(!directoryFormatoEnrolamiento.exists()) {
                if (directoryFormatoEnrolamiento.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            opcionFirmas = tipoFirma;
            tipoFirmaD = Integer.parseInt(tipoFirma);

            if( tipoFirmaD == 0){ tipoFirma="NTRAB";  }
            if( tipoFirmaD == 1){ tipoFirma="FTRAB";  }
            if( tipoFirmaD == 2){ tipoFirma="FPROM";  }

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_FIRM_PKG,ACT_FIRM_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_FIRMA_FOAFI, Integer.parseInt(folioAfi));
            extra.putInt(ACT_FIRMA_FOSER, Integer.parseInt(folioSer));
            extra.putString(ACT_FIRMA_TIPOF, tipoFirma);
            extra.putString(ACT_FIRMA_PERSONAF, opcionFirmas);
            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_FIRMA_ENROLA);
        }

        @JavascriptInterface
        public void llamaComponenteDigitalizador(String folioAfi, String numEmpleado, String folioServ, String tipoProceso, String tipoOperacion) {

            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryDigitalizador= new File(DIR_ACTUAL + "/" + folioAfi , "Digitalizador");

            if(!directoryDigitalizador.exists()) {
                if (directoryDigitalizador.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_DIGI_PKG,ACT_DIGI_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);

            extra.putString(ACT_DIGI_FOAFI, folioAfi +"");
            extra.putString(ACT_DIGI_NOEMP, numEmpleado +"");
            extra.putString(ACT_DIGI_TIPO, "2");
            extra.putString(TAG_RESP_FOLIO, folioServ +"");
            extra.putInt(PARAM_OPCION_TRAMITE, Integer.parseInt(tipoProceso));
            /*if(Integer.parseInt(tipoOperacion) == 26 || Integer.parseInt(tipoOperacion) == 27 || Integer.parseInt(tipoOperacion) == 33){
                extra.putString(ACT_DIGI_TIPO, "1");
                extra.putString(ACT_DIGI_TABLA, "3");
            }else{
                extra.putString(ACT_DIGI_TIPO, "2");
                extra.putString(ACT_DIGI_TABLA, "8");
                extra.putString(ACT_DIGI_FOSER, folioServ + "");
            }*/
            extra.putString(ACT_DIGI_TIPO, "1");
            extra.putString(ACT_DIGI_TABLA, "3");

            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_DIGITALIZADOR);
        }

        @JavascriptInterface
        public void llamaEnrolamiento(String empleado, String curl, String tiposolicitud, String folio) {

           /*LLAMADO COMPONENTE DE ENROLAMIENTO*/

            Intent intent = new Intent(context, WebEnrolamiento.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt("empleado", iEmpleado);
            extra.putString("curl", curl);
            extra.putInt("tiposolicitud", Integer.parseInt(tiposolicitud));
            extra.putInt("folio", Integer.parseInt(folio));
            extra.putString("ipwshuellashc", ipwshuellashc);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_ENROL_FOLIO);

        }

        @JavascriptInterface
        public void llamaComponenteVideo(String nombreVideo, String textoVideo, String iTiempoMinimo, String iTiempoMaximo) {

            writeLog("[nombreVideo] " + nombreVideo);
            writeLog("[textoVideo] " + textoVideo);
            writeLog("[iTiempoMinimo] " + iTiempoMinimo);
            writeLog("[iTiempoMaximo] " + iTiempoMaximo);

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_VIDEO_PKG,ACT_VIDEO_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipOffline);
            extra.putString("nombrevideo", nombreVideo);
            extra.putString("textovideo", textoVideo);
            extra.putString("iTiempoMinimo", iTiempoMinimo);
            extra.putString("iTiempoMaximo", iTiempoMaximo);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_GRABAR_VIDEO);

        }

        @JavascriptInterface
        public void terminarSolConst(int estatusproceso) {

            finish();

        }

        @JavascriptInterface
        public void llamarSolConst(String url) {

            /* DESCARGAR PDF SOL. CONSTANCIA REGISTRO/ TRASPASO */
                url = url.replace("..", "");
            writeLog("URL solConstancia replace" + url);
            /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
            ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);*/
            new WebVisorAfiliacion.DownloadFile().execute( ipOffline + url, "solconstancia.pdf");

            /* LLAMADO COMPONENTE DE IMPRESION DE SOL CONSTANCIA */

            Intent intent = new Intent(context, WebVisorSolConstancia.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt("empleado", iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            startActivity(intent);

        }

        @JavascriptInterface
        public void llamaComponenteEnlace(String url) {

            Intent intent = new Intent(getApplicationContext(), WebVisorEnlace.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_WEB_ENLACE);

        }

        @JavascriptInterface
        public void llamaComponenteEnlaceIne(String url) {

            Intent intent = new Intent(getApplicationContext(), WebVisorEnlace.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_WEB_ENLACE_INE);

        }


    }


    @Override //SE CANCELA CUALQUIER ACCIÓN CON EL BOTÓN DE REGRESAR.
    public void onBackPressed() { }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LLAMAR_FIRMA_ENROLA) {

            //Toast.makeText(this, "LLAMAR_FIRMA_ENROLA", Toast.LENGTH_LONG).show();
            if(resultCode == Activity.RESULT_OK){

                if(data.hasExtra(ACT_FIRMA_NOMFIR)){
                    Bundle firma = data.getExtras();

                    nameFirma = firma.getString(ACT_FIRMA_NOMFIR);

                    soapMoverImagenFirma();

                    // RUTA DE LAS IMAGENES DE LA FIRMA "/sysx/progs/gsoap/wsafiliacionmovil/imagenes"

                }
                else{

                    mWebView.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                    Toast.makeText(this,  R.string.msn_can_firma, Toast.LENGTH_LONG).show();
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                mWebView.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();

            }
        }else if(requestCode == LLAMAR_DIGITALIZADOR) {

            //Toast.makeText(this, "LLAMAR_FIRMA_ENROLA", Toast.LENGTH_LONG).show();
            if(resultCode == Activity.RESULT_OK){
                    String estadoProceso="1";
                    mWebView.loadUrl("javascript:getComponenteDigitalizar('"+ estadoProceso +"')");
                    // RUTA DE LAS IMAGENES DIGITALIZADAS "/sysx/smbx"

            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                String estadoProceso="0";
                mWebView.loadUrl("javascript:getComponenteDigitalizar('"+ estadoProceso +"')");

                Toast.makeText(this,  R.string.msn_can_capdigit, Toast.LENGTH_LONG).show();

            }
        }else if(requestCode == LLAMAR_ENROL_FOLIO) {

            if(resultCode == Activity.RESULT_OK){
                mWebView.loadUrl("javascript:getComponenteEnrolamiento('1')");

            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                 mWebView.loadUrl("javascript:getComponenteEnrolamiento('0')");

            }

        }else if (requestCode == LLAMAR_GRABAR_VIDEO) {
            if(resultCode == Activity.RESULT_OK){
                mWebView.loadUrl("javascript:getComponenteVideo(1)");
            }else if (resultCode == Activity.RESULT_CANCELED) {
                mWebView.loadUrl("javascript:getComponenteVideo(0)");
            }
        }else{
            switch (requestCode){
                case LLAMAR_LECTOR_HUELLAS:
                    String url, ruta;
                    int     OUT_1 = -1;
                    String  OUT_2 = "";
                    JSONObject param;

                    Bundle resApp = data.getExtras();
                    if(resApp != null) {
                        OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);
                        ruta = DIR_EXTERNA+"sys/mem/"+iFolioSolicitud+"/JsonEnviado_"+iFolioSolicitud+".txt";
                        try {
                            OUT_2 = readFile(ruta).trim();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if(opcionManos == 4 || opcionManos == 7){

                        param = prepararJSONEmpleado(OUT_2);
                        url = REST_EMPL + REST_IMGE;
                    }else{
                        param = prepararJSONFingers(OUT_2);
                        url = REST_CLIE + REST_IMGE;
                    }

                    /*writeLog("[Servicio] OUT_2 "+OUT_2);
                    writeLog("[Servicio] url "+url);
                    writeLog("[Servicio] param "+param);*/
                    try{
                        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                                (Request.Method.POST, url, param, response -> {
                                /*if (opcionManos != 4)
                                    guardarTemplatesCliente(response);
                                else
                                    getOnceavaHuella(response);*/

                                    mWebView.loadUrl("javascript:getComponenteHuella('" + data.getExtras().getString("OUT_1") + "', '" + param + "', '" + opcionHuellas +"', '" + response + "')");
                                }, error -> {
                                    writeLog("[Servicio] Error "+error);
                                    MessageDialog aviso = new MessageDialog();
                                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_templates));
                                    showMessage(this, aviso);
                                    mWebView.loadUrl("javascript:getComponenteHuella('0', '0', '" + opcionHuellas +"', '0')");
                                });
                        queue.add(jsObjRequest);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                case LLAMAR_WEB_ENLACE_INE:
                    if(resultCode == Activity.RESULT_OK){

                        mWebView.loadUrl("javascript:getComponenteEnlace('1')");

                    }
                    else if (resultCode == Activity.RESULT_CANCELED) {

                        mWebView.loadUrl("javascript:getComponenteEnlace('0')");

                    }
                    break;

            }
        }
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {

        switch (id){
            case SOAP_MOVER_IMG_FIRMA:

                mWebView.loadUrl("javascript:getComponenteFirma('1', '" + opcionFirmas + "')");

                break;

            default:
                break;
        }

    }

    public void fromHuellasToTemplate(Intent data){

        MessageDialog errorMsn = new MessageDialog();
        int     OUT_1 = -1;
        String  OUT_2 = "";//, OUT_3 = "", OUT_4 = "";

        Bundle resApp = data.getExtras();
        if(resApp != null) {
            OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);
            OUT_2 = resApp.getString (ACT_HUELLAS_OT2);
        }

        switch (OUT_1) {
            case 1: //EXITO EN LA TOMA DE HUELLAS

                JSONObject param;
                String url;

                if(opcionManos == 4 || opcionManos == 7){
                    param = prepararJSONEmpleado(OUT_2);
                    url = REST_EMPL + REST_IMGE;
                }else{
                    param = prepararJSONFingers(OUT_2);
                    url = REST_CLIE + REST_IMGE;
                }

                if(param == null){
                    MessageDialog aviso = new MessageDialog();
                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_excep));
                    showMessage(this, aviso);
                }
                else{
                    //GENERAR TEMPLATES
                    writeLog("[fromHuellasToTemplate] url " + url);
                    writeLog("[fromHuellasToTemplate] param " + param);
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest
                            (Request.Method.POST, url, param, response -> {
                                if (opcionManos != 4)
                                    guardarTemplatesCliente(response);
                                else
                                    getOnceavaHuella(response);
                            }, error -> {
                                writeLog("[fromHuellasToTemplate] " + error);
                                MessageDialog aviso = new MessageDialog();
                                aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_templates));
                                showMessage(this, aviso);
                            });

                    queue.add(jsObjRequest);
                }

                break;

            case 99: //PROCESO CANCELADO
                errorMsn.setDefaultAceptar(getResources().getString(R.string.msn_can_huellas));
                showMessage(this, errorMsn);
                break;
            case 0:
                errorMsn.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_catura));
                showMessage(this, errorMsn);
                break;
            default: //ERROR INESPERADO
                errorMsn.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_otro));
                showMessage(this, errorMsn);
                break;
        }
    }

    public JSONObject prepararJSONEmpleado(String OUT_2){

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();
        String excepcion;

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");



            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                excepcion = finger.getString("ExceptionId");

                if(excepcion.equals("1")) {
                    finger.put("Nfiq", "5");
                }

            }


            param.put("NumberEmp", JSONObject.numberToString(iEmpleado));
            param.put("EmpAutoriza", JSONObject.numberToString(iEmpleado));
            param.put("Fingerprints", arrayEx);
            param.put("Maker", respLector.get("Maker"));
            param.put("Model", respLector.get("Model"));
            param.put("Serial", respLector.get("Serial"));
            param.put("Firmware", respLector.get("Firmware"));
            param.put("FingersException", respLector.get("FingersException"));
            param.put("ExceptionQuality", respLector.get("ExceptionQuality"));

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public JSONObject prepararJSONFingers(String OUT_2){

        int id;
        int nfiq;
        int contEx = 0;
        String excepcion;

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();

        arrFingers = new FingerArray();
        dataFigers = new ArrayList<>();
        String[] nom_dedos = getResources().getStringArray(R.array.enrol_dedos);

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                id          = Integer.parseInt(finger.getString("FingerId"));
                nfiq        = Integer.parseInt(finger.getString("Nfiq"));
                excepcion   = finger.getString("ExceptionId");

                if(nfiq == 5){
                    excepcion = "1";
                }

                if(excepcion.equals("1")) {
                    contEx++;

                    if(opcionManos == HUELLAS_IZQ_FALTA && id > 5)
                        excepcion = "003";
                    else if(opcionManos == HUELLAS_DER_FALTA && id < 6)
                        excepcion = "002";
                    else
                        dataFigers.add(new DataFingerModel(id, nom_dedos[id-1], "0"));

                    finger.put("ExceptionId", excepcion);

                    JSONObject newFinger = new JSONObject(finger.toString());
                    newFinger.put("Nfiq", "5");
                    arrayEx.put(i,newFinger);
                }
                addFinger(finger);
            }

            if(contEx > 7){
                param = null;

            }else{
                param.put("Cliente", JSONObject.numberToString(iFolioSolicitud));
                param.put("Empleado", JSONObject.numberToString(iEmpleado));
                param.put("Fingerprints", arrayEx);
                param.put("FingersException", respLector.get("FingersException"));
                param.put("Maker", respLector.get("Maker"));
                param.put("Model", respLector.get("Model"));
                param.put("Serial", respLector.get("Serial"));
                param.put("Firmware", respLector.get("Firmware"));
                param.put("FingersException", respLector.get("FingersException"));
                param.put("ExceptionQuality", respLector.get("ExceptionQuality"));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public void guardarTemplatesCliente(JSONObject response) {
        try {

            int apiEstatus = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            int apiResultado = response.getInt(REST_RP_RES);

            if (apiEstatus == 201 && apiResultado == 0) {

                writeLog("[guardarTemplatesCliente] " + apiMensaje);

                JSONArray arrTemplates = response.getJSONArray("FingerTemplate");
                JSONObject finger;

                String template;

                for(int k=0; k<arrFingers.getSize(); k++){
                    finger = arrTemplates.getJSONObject(k);
                    template    = finger.getString("Template");

                    arrFingers.setTemplate(k, template);
                }

                //GUARDA TEMPLATE DEL EMPLEADO
                File jsonResp = new File(DIR_ACTUAL, "tempCliente" + todayDate + ".json");
                writeFile(jsonResp, response.toString().getBytes(), false);

                soapFolioEnrolamiento(); //SE GENERA EL FOLIO ENROLAMIENTO
            } else {
                showToast(this,
                        getResources().getString(R.string.msn_err_enr_enrolar));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_ser_con));
        }
    }

    public void getOnceavaHuella(JSONObject response){
        try {

            int apiEstatus = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            int apiResultado = response.getInt(REST_RP_RES);

            if (apiEstatus == 201 && apiResultado == 0) {

                writeLog("[getOnceavaHuella] " + apiMensaje);

                //OBTENER EL TEMPLATE DEL EMPLEADO
                JSONObject fingerEmpleado;
                fingerEmpleado = response.getJSONArray("FingerTemplate").getJSONObject(0);
                fingerEmpleado.put("FingerId", 11);
                //

                //OBTIENE LOS TEMPLATES DEL CLIENTE QUE PREVIAMENTE SE HABÍAN TOMADO
                String stringClientes = readFile(DIR_ACTUAL + "tempCliente" + todayDate + ".json");
                JSONObject templatesCliente = new JSONObject(stringClientes);
                eliminarArchivos();

                JSONObject templates11 = new JSONObject();
                templates11.put("Cliente", iFolioSolicitud + "");

                //ARRAY QUE CONTENDRÁ LOS 11 TEMPLATES A COMPARAR
                JSONArray allFingers = templatesCliente.getJSONArray("FingerTemplate");
                allFingers.put(10, fingerEmpleado); //SE AGREGA EL 11vo TEMPLATE

                templates11.put("FingerTemplate", allFingers);

                compararHuellas(templates11);

            } else {
                showToast(this,
                        getResources().getString(R.string.msn_err_enr_catura));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_ser_con));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addFinger(JSONObject finger) throws JSONException{
        int id;
        int nfiq;
        String base64;
        String excepcion;

        id          = Integer.parseInt(finger.getString("FingerId"));
        nfiq        = Integer.parseInt(finger.getString("Nfiq"));
        base64      = replaceBase64(finger.getString("B64FingerImage"));
        excepcion   = finger.getString("ExceptionId");

        //CREAR EL OBJETO PARA EL DEDO
        arrFingers.newFinger(id, nfiq, base64, excepcion);
    }

    public String replaceBase64(String base64){
        base64 = base64.replace('_','/');

        return base64.replace('-','+');
    }

    public void soapFolioEnrolamiento(){

        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarReenrolamiento");
        sSoap.setParams("sCurp", txtCurpCliente);
        sSoap.setParams("iEmpleado", iEmpleado + "");
        sSoap.setParams("iTipoSol", iTipoSolicitud + "");
        sSoap.setParams("iFolioAfiliacion", iFolioSolicitud + "");

        writeLog("Obtener Folio Enrolamiento [FolioAfiliacion] = [" + iFolioSolicitud + "]");

        CallWebService callWS = new CallWebService(this, SOAP_FOLIO_ENROLAMIENTO);
        callWS.execute(sSoap.getSoapString());
    }

    public void eliminarArchivos(){
        File tempClien = new File(DIR_ACTUAL + "tempCliente" + todayDate + ".json");
        if(tempClien.delete())
            writeLog("[eliminarArchivos] Archivo Eliminado: tempCliente" + todayDate + ".json");
        Log.e("Archivo", "Eliminado");
    }

    public void compararHuellas(JSONObject param){

        String url = REST_CLIE + REST_TENF;

        //GENERAR TEMPLATES
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, param, this::comprobarOnceavaHuella, error -> {
                    writeLog("[compararHuellas] " + error);
                    MessageDialog aviso = new MessageDialog();
                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_compara));
                    showMessage(this, aviso);
                });

        queue.add(jsObjRequest);
    }

    public void comprobarOnceavaHuella(JSONObject response){
        MessageDialog aviso = new MessageDialog();

        try{
            int apiEstatus    = response.getInt(REST_RP_EST);
            String apiMensaje = response.getString(REST_RP_MEN);
            //int apiResultado  = response.getInt(REST_RP_RES);

            if(apiEstatus == 200 || apiEstatus == 201){

                if(apiMensaje.equals("Match")){
                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_match));
                    showMessage(this,aviso);
                }else{
                    soapGenerarFormatoEnrol();
                }

            }else{
                aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_ser_con));
                showMessage(this,aviso);
            }

        }catch(JSONException e){
            aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_ser_res));
            showMessage(this,aviso);
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "solicitudconstancias");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);

            return null;
        }
    }

    public void soapGenerarFormatoEnrol() {
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GenerarFormatoReenrol");
        sSoap.setParams("iFolioEnrol", iFolioEnrolamiento + "");

        writeLog("Generar Formato [Folio Enrolamiento] = [" + iFolioEnrolamiento + "]");

        CallWebService callWS = new CallWebService(this, SOAP_GEN_FORMATO_REENROL);
        callWS.execute(sSoap.getSoapString());
    }

    public void soapMoverImagenFirma(){
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "MoverImagenFirma");
        sSoap.setParams("iOpcion", "1");
        sSoap.setParams("iFolio", iFolioSolicitud + "");
        CallWebService callWS = new CallWebService(this, SOAP_MOVER_IMG_FIRMA);
        callWS.execute(sSoap.getSoapString());
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    private static String readFile(String path) throws IOException {
        FileInputStream stream = new FileInputStream(new File(path));
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /*Instead of using default, pass in a decoder.*/
                return Charset.defaultCharset().decode(bb).toString();
        }
        finally {
            stream.close();
        }
    }

    public void finalizarActividad(){
        writeLog("Afiliacion Cancelada");
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }

    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {
            }
        }
    }

}

