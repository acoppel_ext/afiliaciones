package com.aforecoppel.afiliaciontrabajador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.*;

public class WebVisorCaptura extends AppCompatActivity {
    private WebView mWebViewCaptura;
    private String todayDate;
    private String DIR_ACTUAL;
    private String ipAddress, ipServidor;
    private int iEmpleado;
    private String txtCurpCliente;
    private int iTipoSolicitud;
    private int iFolioSolicitud;
    private int iFolioEnrolamiento;
    private int opcionManos; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACION
    private int opcionHuellas;
    private FingerArray arrFingers;
    private ArrayList<DataFingerModel> dataFigers;
    private String ipOffline = "";

    //VARIABLES DE LA ACTIVIDAD
    private RequestQueue queue;

    public static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor_captura);
        queue = Volley.newRequestQueue(this);

        //VALORES QUE LLEGAN DEL SUBMENÚ
        Intent intent = getIntent();
        getParameters(intent);

        NukeSSLCerts ObjectNukeSSL = new NukeSSLCerts();
        ObjectNukeSSL.nuke();

        if (!isOnline()) {

            Intent intentError = new Intent(this, errorConexion.class);
            this.startActivity(intentError);

        } else {
            /* Para generar el template */

            Date sysDate = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
            todayDate = dateFormat.format(sysDate);

            mWebViewCaptura = (WebView) findViewById(R.id.webviewcaptura);
            WebSettings mWebSettings = mWebViewCaptura.getSettings();

            mWebSettings.setJavaScriptEnabled(true);
            JavaScriptInterface jsInterface = new JavaScriptInterface();
            mWebViewCaptura.addJavascriptInterface(jsInterface, "Android");

            /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
            ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);*/
            mWebViewCaptura.loadUrl( ipOffline + "/capturaafiliacion/indexcapturaafiliacion.html?empleado=" + iEmpleado );


        }
    }

    @SuppressLint("JavascriptInterface")
    public class JavaScriptInterface {

        @JavascriptInterface
        public void volverMenu(String cerrar) {
            finish();
        }

        @JavascriptInterface
        public void llamaComponenteEnlace(String url) {

            Intent intent = new Intent(getApplicationContext(), WebVisorEnlace.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_WEB_ENLACE);

        }

        @JavascriptInterface
        public void llamaComponenteHuellas(String tipoCaptura, String identificador, String band, String opcion, String empleado, String tiposolicitud, String curp) {

            txtCurpCliente = curp;

            /* DIRECTORIOS DE COMPLEMENTOS */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, identificador+"");
            iFolioSolicitud = Integer.parseInt(identificador);
            iFolioEnrolamiento = Integer.parseInt(identificador);
            opcionManos = Integer.parseInt(tipoCaptura);

            /* Opcion de Huellas */
            opcionHuellas=Integer.parseInt(opcion);
            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE HUELLAS*/
            Intent intent = new Intent(getApplicationContext(), MainActivityEnrollMovil.class);

            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, Integer.parseInt(tipoCaptura)); // *** Consultar *** opcionManos
            extra.putInt(ACT_HUELLAS_IMG, 1); //Dejar archivo de huellas
            extra.putInt(ACT_HUELLAS_NUM, Integer.parseInt(identificador)); //numeroFolio
            intent.putExtras(extra);

            startActivityForResult(intent, 100);

        }


    }

    /*@Override
    public void onBackPressed() { }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case LLAMAR_LECTOR_HUELLAS:
                String url, ruta;
                int     OUT_1 = -1;
                String  OUT_2 = "";
                JSONObject param;

                Bundle resApp = data.getExtras();
                if(resApp != null) {
                    OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);
                    ruta = DIR_EXTERNA+"sys/mem/"+iFolioSolicitud+"/JsonEnviado_"+iFolioSolicitud+".txt";
                    OUT_2 = readFile(ruta).trim();
                }

                if(opcionManos == 4 || opcionManos == 7){
                    param = prepararJSONEmpleado(OUT_2);
                    url = REST_EMPL + REST_IMGE;
                }else{
                    param = prepararJSONFingers(OUT_2);
                    url = REST_CLIE + REST_IMGE;
                }
                try{
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest
                            (Request.Method.POST, url, param, response -> {
                                mWebViewCaptura.loadUrl("javascript:getComponenteHuella('1', '" + data.getExtras().getString("OUT_2") + "', '" + opcionHuellas +"', '" + response + "')");
                            }, error -> {
                                mWebViewCaptura.loadUrl("javascript:getComponenteHuella('0', '0', '" + opcionHuellas +"', '0')");
                                /*MessageDialog aviso = new MessageDialog();
                                aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_templates));
                                showMessage(this, aviso);*/
                            });
                    queue.add(jsObjRequest);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;

        }

    }

    public JSONObject prepararJSONEmpleado(String OUT_2){

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();
        String excepcion;

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                excepcion = finger.getString("ExceptionId");

                if(excepcion.equals("1")) {
                    finger.put("Nfiq", "5");
                }

            }

            param.put("NumberEmp", JSONObject.numberToString(iEmpleado));
            param.put("EmpAutoriza", JSONObject.numberToString(iEmpleado));
            param.put("Fingerprints", arrayEx);
            param.put("Maker", respLector.get("Maker"));
            param.put("Model", respLector.get("Model"));
            param.put("Serial", respLector.get("Serial"));
            param.put("Firmware", respLector.get("Firmware"));
            param.put("FingersException", respLector.get("FingersException"));
            param.put("ExceptionQuality", respLector.get("ExceptionQuality"));

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public JSONObject prepararJSONFingers(String OUT_2){

        int id;
        int nfiq;
        int contEx = 0;
        String excepcion;

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();

        arrFingers = new FingerArray();
        dataFigers = new ArrayList<>();
        String[] nom_dedos = getResources().getStringArray(R.array.enrol_dedos);

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                id          = Integer.parseInt(finger.getString("FingerId"));
                nfiq        = Integer.parseInt(finger.getString("Nfiq"));
                excepcion   = finger.getString("ExceptionId");

                if(nfiq == 5){
                    excepcion = "1";
                }

                if(excepcion.equals("1")) {
                    contEx++;

                    if(opcionManos == HUELLAS_IZQ_FALTA && id > 5)
                        excepcion = "003";
                    else if(opcionManos == HUELLAS_DER_FALTA && id < 6)
                        excepcion = "002";
                    else
                        dataFigers.add(new DataFingerModel(id, nom_dedos[id-1], "0"));

                    finger.put("ExceptionId", excepcion);

                    JSONObject newFinger = new JSONObject(finger.toString());
                    newFinger.put("Nfiq", "5");
                    arrayEx.put(i,newFinger);
                }
                addFinger(finger);
            }

            if(contEx > 7){
                param = null;

            }else{
                param.put("Cliente", JSONObject.numberToString(iFolioSolicitud));
                param.put("Empleado", JSONObject.numberToString(iEmpleado));
                param.put("Fingerprints", arrayEx);
                param.put("FingersException", respLector.get("FingersException"));
                param.put("Maker", respLector.get("Maker"));
                param.put("Model", respLector.get("Model"));
                param.put("Serial", respLector.get("Serial"));
                param.put("Firmware", respLector.get("Firmware"));
                param.put("FingersException", respLector.get("FingersException"));
                param.put("ExceptionQuality", respLector.get("ExceptionQuality"));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public void addFinger(JSONObject finger) throws JSONException{
        int id;
        int nfiq;
        String base64;
        String excepcion;

        id          = Integer.parseInt(finger.getString("FingerId"));
        nfiq        = Integer.parseInt(finger.getString("Nfiq"));
        base64      = replaceBase64(finger.getString("B64FingerImage"));
        excepcion   = finger.getString("ExceptionId");

        //CREAR EL OBJETO PARA EL DEDO
        arrFingers.newFinger(id, nfiq, base64, excepcion);
    }

    public String replaceBase64(String base64){
        base64 = base64.replace('_','/');

        return base64.replace('-','+');
    }


    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            ipOffline = intent.getStringExtra("ipOffline");
            iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);

        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }

    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
            } catch (Exception e) {
            }
        }
    }
}
