package com.aforecoppel.afiliaciontrabajador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_AFILIACION_NUM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_CLS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_FOAFI;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_NOEMP;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_PKG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_TABLA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_TIPO;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_FOAFI;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_FOSER;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_NOMFIR;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_TIPOF;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRM_CLS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRM_PKG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_IMG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_NUM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_OPC;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_OT1;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.CONFIG_KEY1;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_EXTERNA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_REENROL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.HUELLAS_DER_FALTA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.HUELLAS_IZQ_FALTA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_DIGITALIZADOR;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_FIRMA_ENROLA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_LECTOR_HUELLAS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_WEB_ENLACE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.PARAM_OPCION_TRAMITE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_CLIE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_EMPL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_IMGE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.SOAP_MOVER_IMG_FIRMA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.TAG_RESP_FOLIO;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.readFile;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.showMessage;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.showToast;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.writeLog;

public class WebVisorDoctorn extends AppCompatActivity implements View.OnClickListener{

    private String ipAddress;
    private String sUrl;
    private WebView mWebViewDoctorn;
    public static Context context;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        writeLog("onCreate  WebVisorDoctorn ");

        setContentView(R.layout.activity_web_visor_doctorn);
        Intent intent = getIntent();
        getParameters(intent);
        context = this;
        setComponentes();
        setListeners();

        mWebViewDoctorn = (WebView) findViewById(R.id.webviewdoctorn);
        WebSettings mWebSettings = mWebViewDoctorn.getSettings();

        mWebSettings.setJavaScriptEnabled(true);

        writeLog(" WebVisorDoctorn " + sUrl);
        /*JavaScriptInterface jsInterface = new JavaScriptInterface();
        mWebViewDoctorn.addJavascriptInterface(jsInterface, "Android");
        mWebViewDoctorn.loadUrl(sUrl);*/
    }

    public void setComponentes(){
        writeLog("setComponentes  WebVisorDoctorn ");
        btnRegresar = findViewById(R.id.btn_regresar_touch);

    }

    public void setListeners(){

        writeLog("setListeners  WebVisorDoctorn ");
        btnRegresar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == btnRegresar.getId()){
            setResult(Activity.RESULT_OK);
            finishAndRemoveTask();
        }

    }

    public void getParameters(Intent intent){

        writeLog("getParameters  WebVisorDoctorn ");
        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            sUrl = intent.getStringExtra("url");
            new CallWebService(ipAddress);

        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }


    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }

}
