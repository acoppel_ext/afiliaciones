package com.aforecoppel.afiliaciontrabajador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_AFILIACION_NUM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_CLS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_FOAFI;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_FOSER;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_NOEMP;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_PKG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_TABLA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_DIGI_TIPO;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_FOAFI;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_FOSER;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_NOMFIR;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_PERSONAF;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRMA_TIPOF;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRM_CLS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FIRM_PKG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_CLS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_IMG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_NUM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_OPC;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_OT1;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_OT2;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_HUELLAS_PKG;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_TIPO_AFI;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.CONFIG_KEY1;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_AFILIACION;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_EXTERNA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_REENROL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.HUELLAS_DER_FALTA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.HUELLAS_IZQ_FALTA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_DIGITALIZADOR;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_FIRMA_ENROLA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_LECTOR_HUELLAS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_WEB_ENLACE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.LLAMAR_WEB_DOCTORN;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.PARAM_OPCION_TRAMITE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_CLIE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_EMPL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_IMGE;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_RP_EST;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_RP_MEN;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.REST_RP_RES;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.SOAP_MOVER_IMG_FIRMA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.TAG_RESP_FOLIO;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.readFile;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.showMessage;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.showToast;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.writeFile;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.writeLog;

public class WebVisorEnlace extends AppCompatActivity implements AsyncTaskCompleteListener<String>{

    private String ipAddress;
    private int iEmpleado;
    private String sUrl;
    private WebView mWebViewEnlace;
    private String txtCurpCliente;
    private int iTipoSolicitud;
    private String DIR_ACTUAL;
    private int iFolioSolicitud;
    private int iFolioEnrolamiento;
    private int opcionManos; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACION
    private int opcionHuellas;
    private FingerArray arrFingers;
    private ArrayList<DataFingerModel> dataFigers;
    //VARIABLES DE LA ACTIVIDAD
    private RequestQueue queue;
    private int tipoFirmaD;
    private String opcionFirmas;
    public static Context context;
    private String nameFirma = "";
    private String sFolioIne = "0";
    private String ipOffline = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor_enlace);
        Intent intent = getIntent();
        getParameters(intent);
        context = this;
        queue = Volley.newRequestQueue(this);

        DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;

        mWebViewEnlace = (WebView) findViewById(R.id.webviewenlace);
        WebSettings mWebSettings = mWebViewEnlace.getSettings();

        mWebSettings.setJavaScriptEnabled(true);
        JavaScriptInterface jsInterface = new JavaScriptInterface();
        mWebViewEnlace.addJavascriptInterface(jsInterface, "Android");
        mWebViewEnlace.loadUrl(sUrl);
    }

    @SuppressLint("JavascriptInterface")
    public class JavaScriptInterface {

        @JavascriptInterface
        public void volverMenu(String cerrar) {
            writeLog("VolverMenu Enlace");
            finish();
        }

        @JavascriptInterface
        public void volverMenuIne(String cerrar) {

            writeLog("cerrar " + cerrar);

            if(Integer.parseInt(cerrar) == 0){
                writeLog("VolverMenu Enlace 0");
                //mWebViewEnlace.loadUrl("javascript:getComponenteEnlace('0')");
                setResult(Activity.RESULT_CANCELED);
                finish();
            }else if(Integer.parseInt(cerrar) == 1){
                writeLog("VolverMenu Enlace 1");
                //mWebViewEnlace.loadUrl("javascript:getComponenteEnlace('1')");
                setResult(Activity.RESULT_OK);
                finish();
            }else{
                writeLog("VolverMenu Enlace else");
                //mWebViewEnlace.loadUrl("javascript:getComponenteEnlace('0')");
                setResult(Activity.RESULT_CANCELED);
                finish();
            }


        }

        @JavascriptInterface
        public void llamaComponenteHuellas(String tipoCaptura, String identificador, String band, String opcion, String empleado, String tiposolicitud, String curp) {

            /* Para generar el template */
            //iEmpleado = empleado;
            txtCurpCliente = curp;

            /* DIRECTORIOS DE COMPLEMENTOS */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, identificador+"");
            iFolioSolicitud = Integer.parseInt(identificador);

            iFolioEnrolamiento = Integer.parseInt(identificador);
            opcionManos = Integer.parseInt(tipoCaptura);

            /* Opcion de Huellas */
            opcionHuellas=Integer.parseInt(opcion);
            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE HUELLAS*/
            Intent intent = new Intent(WebVisorEnlace.this, MainActivityEnrollMovil.class);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

            // intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, Integer.parseInt(tipoCaptura)); // *** Consultar *** opcionManos
            extra.putInt(ACT_HUELLAS_IMG, 1); //Dejar archivo de huellas
            extra.putInt(ACT_HUELLAS_NUM, Integer.parseInt(identificador)); //numeroFolio
            intent.putExtras(extra);
            startActivityForResult(intent, 100);

        }


        @JavascriptInterface
        public void llamaComponenteHuellasIne(String tipoCaptura, String identificador, String band, String opcion, String empleado, String tiposolicitud, String curp) {

            /* Para generar el template */
            //iEmpleado = empleado;
            txtCurpCliente = curp;

             /* DIRECTORIOS DE COMPLEMENTOS */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, identificador+"");
            iFolioSolicitud = Integer.parseInt(identificador);
            sFolioIne = identificador;
            iFolioEnrolamiento = Integer.parseInt(identificador);
            opcionManos = Integer.parseInt(tipoCaptura);

            /* Opcion de Huellas */
            opcionHuellas=Integer.parseInt(opcion);
            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE HUELLAS*/
            Intent intent = new Intent(WebVisorEnlace.this, MainActivityEnrollMovil.class);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP | intent.FLAG_ACTIVITY_SINGLE_TOP);

            // intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));

            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, Integer.parseInt(tipoCaptura)); // *** Consultar *** opcionManos
            extra.putInt(ACT_HUELLAS_IMG, 1); //Dejar archivo de huellas
            extra.putString(ACT_HUELLAS_NUM, identificador + ""); //numeroFolio
            intent.putExtras(extra);
            startActivityForResult(intent, 100);

        }

        @JavascriptInterface
        public void llamaComponenteDigitalizador(String folioAfi, String numEmpleado, String folioServ, String tipoProceso, String tipoOperacion) {

            writeLog(" folioAfi " + folioAfi);
            writeLog(" numEmpleado " + numEmpleado);
            writeLog(" folioServ " + folioServ);
            writeLog(" tipoProceso " + tipoProceso);
            writeLog(" tipoOperacion " + tipoOperacion);

            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryDigitalizador= new File(DIR_ACTUAL + "/" + folioAfi , "Digitalizador");

            if(!directoryDigitalizador.exists()) {
                if (directoryDigitalizador.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_DIGI_PKG,ACT_DIGI_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putString(ACT_DIGI_FOAFI, folioAfi +"");
            extra.putString(ACT_DIGI_NOEMP, numEmpleado +"");
            extra.putString(ACT_DIGI_TIPO, "2");
            extra.putString(TAG_RESP_FOLIO, folioServ +"");
            extra.putInt(PARAM_OPCION_TRAMITE, Integer.parseInt(tipoProceso));
            extra.putString(ACT_DIGI_TIPO, "1");
            extra.putString(ACT_DIGI_TABLA, "3");

            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_DIGITALIZADOR);
        }

        @JavascriptInterface
        public void llamaComponenteFirmaD(String folioAfi, String folioSer, String tipoFirma) {

            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            iFolioSolicitud = Integer.parseInt(folioAfi);
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryFormatoEnrolamiento= new File(DIR_ACTUAL + "/" + folioAfi , "FormatoEnrolamiento");

            if(!directoryFormatoEnrolamiento.exists()) {
                if (directoryFormatoEnrolamiento.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            opcionFirmas = tipoFirma;
            tipoFirmaD = Integer.parseInt(tipoFirma);

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_FIRM_PKG,ACT_FIRM_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_FIRMA_FOAFI, Integer.parseInt(folioAfi));
            extra.putInt(ACT_FIRMA_FOSER, Integer.parseInt(folioSer));
            extra.putString(ACT_FIRMA_TIPOF, "IMPRE");
            extra.putString(ACT_FIRMA_PERSONAF, opcionFirmas);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_FIRMA_ENROLA);

        }

        @JavascriptInterface
        public void llamarSolConst(String url) {

            url = url.replace("..", "");
            url = url.replace(ipOffline, "");

            Intent intent = new Intent(context, WebVisorSolConstancia.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt("empleado", iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            startActivity(intent);

        }

        @JavascriptInterface
        public void llamaComponenteEnlace(String url) {

            Intent intent = new Intent(getApplicationContext(), WebVisorEnlace.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_WEB_ENLACE);

        }


        @JavascriptInterface
        public void llamaComponenteDoctorn(String url) {

            Intent intent = new Intent(getApplicationContext(), WebVisorEnlace.class);
            Bundle extra = new Bundle();

            writeLog(" llamaComponenteDoctorn ");
            writeLog(" URL " + url);

            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_AFILIACION_NUM, iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);
            intent.putExtras(extra);
            startActivityForResult(intent, LLAMAR_WEB_DOCTORN);

        }

    }

    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            ipOffline = intent.getStringExtra("ipOffline");
            iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);
            sUrl = intent.getStringExtra("url");
            writeLog(" sUrl ENLACE " + sUrl);

            new CallWebService(ipAddress);

        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case LLAMAR_LECTOR_HUELLAS:
                String url, ruta;
                int     OUT_1 = -1;
                String  OUT_2 = "";
                JSONObject param;

                Bundle resApp = data.getExtras();
                if(resApp != null) {
                    OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);

                    if(sFolioIne != "0"){
                        ruta = DIR_EXTERNA+"sys/mem/"+sFolioIne+"/JsonEnviado_"+sFolioIne+".txt";
                    }else{
                        ruta = DIR_EXTERNA+"sys/mem/"+iFolioSolicitud+"/JsonEnviado_"+iFolioSolicitud+".txt";
                    }

                    OUT_2 = readFile(ruta).trim();

                    //OUT_2 = resApp.getString (ACT_HUELLAS_OT2);
                }

                if(sFolioIne == "0") {

                    if (opcionManos == 4 || opcionManos == 7) {
                        param = prepararJSONEmpleado(OUT_2);
                        url = REST_EMPL + REST_IMGE;
                    } else {
                        param = prepararJSONFingers(OUT_2);
                        url = REST_CLIE + REST_IMGE;
                    }
                    try {
                        writeLog(" url " + url);
                        //writeLog(" param " + param);

                        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                                (Request.Method.POST, url, param, response -> {
                                    mWebViewEnlace.loadUrl("javascript:getComponenteHuella('1', '" + param + "', '" + opcionHuellas +"', '" + response + "')");
                                }, error -> {
                                    mWebViewEnlace.loadUrl("javascript:getComponenteHuella('0', '0', '" + opcionHuellas +"', '0')");
                                    /*MessageDialog aviso = new MessageDialog();
                                    aviso.setDefaultAceptar(getResources().getString(R.string.msn_err_enr_templates));
                                    showMessage(this, aviso);*/
                                });
                        queue.add(jsObjRequest);
                    } catch (Exception e) {
                        writeLog(" ERROR " + e.getStackTrace().toString());
                        e.printStackTrace();
                    }
                }else{
                    JSONArray arrayEx;
                    JSONObject finger; //Objeto para tomar cada uno de los finger
                    String indiceIzq="", indiceDerec="";


                    try {
                        JSONObject respLector = new JSONObject(OUT_2);
                        arrayEx = respLector.getJSONArray("Fingerprints");
                        for(int i=0; i<arrayEx.length(); i++) {
                            finger = arrayEx.getJSONObject(i);

                            if(Integer.parseInt(finger.getString("FingerId")) == 2){
                                indiceDerec = finger.getString("B64FingerImage");
                            }
                            if(Integer.parseInt(finger.getString("FingerId")) == 7){
                                indiceIzq = finger.getString("B64FingerImage");
                            }

                        }

                        mWebViewEnlace.loadUrl("javascript:getComponenteHuellaIne('" + indiceDerec + "', '" + indiceIzq + "')");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case LLAMAR_DIGITALIZADOR:

                if(resultCode == Activity.RESULT_OK){
                    String estadoProceso="1";
                    mWebViewEnlace.loadUrl("javascript:getComponenteDigitalizar('"+ estadoProceso +"')");
                    // RUTA DE LAS IMAGENES DIGITALIZADAS "/sysx/smbx"

                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    String estadoProceso="0";
                    mWebViewEnlace.loadUrl("javascript:getComponenteDigitalizar('"+ estadoProceso +"')");

                    Toast.makeText(this,  R.string.msn_can_capdigit, Toast.LENGTH_LONG).show();

                }

                break;
            case LLAMAR_WEB_DOCTORN:

                if(resultCode == Activity.RESULT_OK){
                    String estadoProceso="1";
                    mWebViewEnlace.loadUrl("javascript:getComponenteDoctorn('"+ estadoProceso +"')");
                    // RUTA DE LAS IMAGENES DIGITALIZADAS "/sysx/smbx"

                }
                else if (resultCode == Activity.RESULT_CANCELED) {
                    String estadoProceso="0";
                    mWebViewEnlace.loadUrl("javascript:getComponenteDoctorn('"+ estadoProceso +"')");

                    //Toast.makeText(this,  R.string.msn_can_capdigit, Toast.LENGTH_LONG).show();

                }

                break;
            case LLAMAR_FIRMA_ENROLA:
                if(resultCode == Activity.RESULT_OK){

                    if(data.hasExtra(ACT_FIRMA_NOMFIR)){
                        Bundle firma = data.getExtras();

                        nameFirma = firma.getString(ACT_FIRMA_NOMFIR);

                        soapMoverImagenFirma();

                        // RUTA DE LAS IMAGENES DE LA FIRMA "/sysx/progs/gsoap/wsafiliacionmovil/imagenes"

                    }
                    else{

                        mWebViewEnlace.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");

                        Toast.makeText(this,  R.string.msn_can_firma, Toast.LENGTH_LONG).show();
                    }
                }
                else if (resultCode == Activity.RESULT_CANCELED) {

                    mWebViewEnlace.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                    Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();

                }else{
                    mWebViewEnlace.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                    Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {

        switch (id){
            case SOAP_MOVER_IMG_FIRMA:

                mWebViewEnlace.loadUrl("javascript:getComponenteFirma('1', '" + opcionFirmas + "')");

                break;

            default:
                break;
        }

    }

    public void soapMoverImagenFirma(){
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "MoverImagenFirma");
        sSoap.setParams("iOpcion", "1");
        sSoap.setParams("iFolio", iFolioSolicitud + "");
        CallWebService callWS = new CallWebService(this, SOAP_MOVER_IMG_FIRMA);
        callWS.execute(sSoap.getSoapString());
    }


    public JSONObject prepararJSONEmpleado(String OUT_2){

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();
        String excepcion;

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                excepcion = finger.getString("ExceptionId");

                if(excepcion.equals("1")) {
                    finger.put("Nfiq", "5");
                }

            }

            param.put("NumberEmp", JSONObject.numberToString(iEmpleado));
            param.put("EmpAutoriza", JSONObject.numberToString(iEmpleado));
            param.put("Fingerprints", arrayEx);
            param.put("Maker", respLector.get("Maker"));
            param.put("Model", respLector.get("Model"));
            param.put("Serial", respLector.get("Serial"));
            param.put("Firmware", respLector.get("Firmware"));
            param.put("FingersException", respLector.get("FingersException"));
            param.put("ExceptionQuality", respLector.get("ExceptionQuality"));

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public JSONObject prepararJSONFingers(String OUT_2){

        int id;
        int nfiq;
        int contEx = 0;
        String excepcion;

        JSONArray arrayEx;
        JSONObject finger; //Objeto para tomar cada uno de los finger
        JSONObject param = new JSONObject();

        arrFingers = new FingerArray();
        dataFigers = new ArrayList<>();
        String[] nom_dedos = getResources().getStringArray(R.array.enrol_dedos);

        try {
            JSONObject respLector = new JSONObject(OUT_2);
            arrayEx = respLector.getJSONArray("Fingerprints");

            for(int i=0; i<arrayEx.length(); i++){
                finger = arrayEx.getJSONObject(i);

                id          = Integer.parseInt(finger.getString("FingerId"));
                nfiq        = Integer.parseInt(finger.getString("Nfiq"));
                excepcion   = finger.getString("ExceptionId");

                if(nfiq == 5){
                    excepcion = "1";
                }

                if(excepcion.equals("1")) {
                    contEx++;

                    if(opcionManos == HUELLAS_IZQ_FALTA && id > 5)
                        excepcion = "003";
                    else if(opcionManos == HUELLAS_DER_FALTA && id < 6)
                        excepcion = "002";
                    else
                        dataFigers.add(new DataFingerModel(id, nom_dedos[id-1], "0"));

                    finger.put("ExceptionId", excepcion);

                    JSONObject newFinger = new JSONObject(finger.toString());
                    newFinger.put("Nfiq", "5");
                    arrayEx.put(i,newFinger);
                }
                addFinger(finger);
            }

            if(contEx > 7 && sFolioIne == "0"){
                param = null;

            }else{
                param.put("Cliente", JSONObject.numberToString(iFolioSolicitud));
                param.put("Empleado", JSONObject.numberToString(iEmpleado));
                param.put("Fingerprints", arrayEx);
                param.put("FingersException", respLector.get("FingersException"));
                param.put("Maker", respLector.get("Maker"));
                param.put("Model", respLector.get("Model"));
                param.put("Serial", respLector.get("Serial"));
                param.put("Firmware", respLector.get("Firmware"));
                param.put("FingersException", respLector.get("FingersException"));
                param.put("ExceptionQuality", respLector.get("ExceptionQuality"));
            }

        } catch (JSONException e) {
            showToast(this,
                    getResources().getString(R.string.msn_err_enr_res_lec));
        }

        return param;
    }

    public void addFinger(JSONObject finger) throws JSONException{
        int id;
        int nfiq;
        String base64;
        String excepcion;

        id          = Integer.parseInt(finger.getString("FingerId"));
        nfiq        = Integer.parseInt(finger.getString("Nfiq"));
        base64      = replaceBase64(finger.getString("B64FingerImage"));
        excepcion   = finger.getString("ExceptionId");

        //CREAR EL OBJETO PARA EL DEDO
        arrFingers.newFinger(id, nfiq, base64, excepcion);
    }

    public String replaceBase64(String base64){
        base64 = base64.replace('_','/');

        return base64.replace('-','+');
    }


    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }
    @Override //SE CANCELA CUALQUIER ACCIÓN CON EL BOTÓN DE REGRESAR.
    public void onBackPressed() { }

}
