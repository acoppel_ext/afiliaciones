package com.aforecoppel.afiliaciontrabajador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.*;

public class WebVisorImpresion extends AppCompatActivity implements AsyncTaskCompleteListener<String>{
    private String ipAddress, ipServidor;
    private int iEmpleado;
    private WebView mWebViewImpresion;
    private String DIR_ACTUAL;
    private String opcionFirmas;
    private String nameFirma = "";
    private int iFolioSolicitud;
    private int tipoFirmaD;
    public static Context context;
    private String ipOffline = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor_impresion);

        context = this;

        //VALORES QUE LLEGAN DEL SUBMENÚ
        Intent intent = getIntent();
        getParameters(intent);

        if (!isOnline()) {

            Intent intentError = new Intent(this, errorConexion.class);
            this.startActivity(intentError);

        } else {
            mWebViewImpresion = (WebView) findViewById(R.id.webviewimpresion);
            WebSettings mWebSettings = mWebViewImpresion.getSettings();

            mWebSettings.setJavaScriptEnabled(true);
            WebVisorImpresion.JavaScriptInterface jsInterface = new WebVisorImpresion.JavaScriptInterface();
            mWebViewImpresion.addJavascriptInterface(jsInterface, "Android");
            /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
            ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);*/
            mWebViewImpresion.loadUrl(ipOffline + "/constanciasafiliacion/indexConstanciasImpresion.html?empleado=" + iEmpleado);

        }
    }

    @SuppressLint("JavascriptInterface")
    public class JavaScriptInterface {

        @JavascriptInterface
        public void terminarSolConst(int estatusproceso) {

            finish();

        }

        @JavascriptInterface
        public void llamaComponenteFirmaDigital(String folioAfi, String folioSer, String tipoFirma) {
            iFolioSolicitud = Integer.parseInt(folioAfi);
            /* DIRECTORIO DE COMPLEMENTO */
            DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;
            File directory= new File(DIR_ACTUAL, folioAfi+"");

            if(!directory.exists()) {
                if (directory.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            File directoryFormatoEnrolamiento= new File(DIR_ACTUAL + "/" + folioAfi , "FormatoEnrolamiento");

            if(!directoryFormatoEnrolamiento.exists()) {
                if (directoryFormatoEnrolamiento.mkdirs()) {
                    Log.i("MkDirs", "Carpeta creada Folio");
                }
            }

            opcionFirmas = tipoFirma;
            tipoFirmaD = Integer.parseInt(tipoFirma);

            if( tipoFirmaD == 0){ tipoFirma="NTRAB";  }
            if( tipoFirmaD == 1){ tipoFirma="FTRAB";  }


            File file = new File(DIR_ACTUAL  + "/" + folioAfi + "/FormatoEnrolamiento/FARE_" + folioAfi +"_" + tipoFirma +".JPG");
            file.delete();

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_FIRM_PKG,ACT_FIRM_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt(ACT_FIRMA_FOAFI, Integer.parseInt(folioAfi));
            extra.putInt(ACT_FIRMA_FOSER, 0);
            extra.putString(ACT_FIRMA_TIPOF, tipoFirma);
            extra.putString(ACT_FIRMA_PERSONAF, opcionFirmas);
            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_FIRMA_ENROLA);
        }

        @JavascriptInterface
        public void llamaComponenteVideo(String nombreVideo, String textoVideo) {

            /*LLAMADO COMPONENTE DE FIRMA*/
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(ACT_VIDEO_PKG,ACT_VIDEO_CLS));

            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipServidor);
            extra.putString("nombrevideo", nombreVideo);
            extra.putString("textovideo", textoVideo);
            intent.putExtras(extra);

            startActivityForResult(intent, LLAMAR_GRABAR_VIDEO);
        }

        @JavascriptInterface
        public void llamarSolConst(String url) {

            /* DESCARGAR PDF SOL. CONSTANCIA REGISTRO/ TRASPASO */
            url = url.replace("..", "");
            /*ipServidor = ipAddress.replace(IP_SERV_MOVIL,IP_SERV_DEV);
            ipServidor = ipServidor.replace(IP_SERV_QA_WS,IP_SERV_QA);*/
            new WebVisorImpresion.DownloadFile().execute( ipOffline +url, "solconstancia.pdf");

            /* LLAMADO COMPONENTE DE IMPRESION DE SOL CONSTANCIA */

            Intent intent = new Intent(context, WebVisorSolConstancia.class);
            Bundle extra = new Bundle();
            extra.putString(CONFIG_KEY1, ipAddress);
            extra.putString("ipOffline", ipOffline);
            extra.putInt("empleado", iEmpleado);
            extra.putString("url", url);
            intent.putExtras(extra);

            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            startActivity(intent);
                        }
                    },
                    5000
            );



        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LLAMAR_FIRMA_ENROLA) {

            //Toast.makeText(this, "LLAMAR_FIRMA_ENROLA", Toast.LENGTH_LONG).show();
            if(resultCode == Activity.RESULT_OK){

                if(data.hasExtra(ACT_FIRMA_NOMFIR)){
                    Bundle firma = data.getExtras();

                    nameFirma = firma.getString(ACT_FIRMA_NOMFIR);
                    soapMoverImagenFirma();
                    // RUTA DE LAS IMAGENES DE LA FIRMA "/sysx/progs/gsoap/wsafiliacionmovil/imagenes"

                }
                else{
                    Toast.makeText(this,  R.string.msn_can_firma, Toast.LENGTH_LONG).show();
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                mWebViewImpresion.loadUrl("javascript:getComponenteFirma('0', '" + opcionFirmas + "')");
                Toast.makeText(this,  R.string.msn_can_capfirma, Toast.LENGTH_LONG).show();

            }
        }

        if (requestCode == LLAMAR_GRABAR_VIDEO) {
            if(resultCode == Activity.RESULT_OK){
                mWebViewImpresion.loadUrl("javascript:getComponenteVideo(1)");
            }else if (resultCode == Activity.RESULT_CANCELED) {
                mWebViewImpresion.loadUrl("javascript:getComponenteVideo(0)");
            }

        }

    }

    public void soapMoverImagenFirma(){
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "MoverImagenFirma");
        sSoap.setParams("iOpcion", "1");
        sSoap.setParams("iFolio", iFolioSolicitud + "");
        CallWebService callWS = new CallWebService(this, SOAP_MOVER_IMG_FIRMA);
        callWS.execute(sSoap.getSoapString());
    }

    @Override
    public void onTaskComplete(String xmlMen, int id) {

        switch (id){
            case SOAP_MOVER_IMG_FIRMA:

                mWebViewImpresion.loadUrl("javascript:getComponenteFirma('1', '" + opcionFirmas + "')");

                break;

            default:
                break;
        }

    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "solicitudconstancias");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);

            return null;
        }
    }

    @Override
    public void onBackPressed() { }

    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            ipOffline = intent.getStringExtra("ipOffline");
            iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);
            new CallWebService(ipAddress);
        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }

    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }


    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

}
