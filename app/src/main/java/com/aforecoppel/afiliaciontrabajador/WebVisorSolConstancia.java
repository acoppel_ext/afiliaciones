package com.aforecoppel.afiliaciontrabajador;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.pdf.PdfRenderer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_AFILIACION_NUM;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.ACT_FORMATO_PDF;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.CONFIG_KEY1;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_AFILIACION;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_EXTERNA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.DIR_FORMATO;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.IP_SERV_DEV;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.IP_SERV_MOVIL;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.IP_SERV_QA;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.IP_SERV_QA_WS;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.showMessage;
import static com.aforecoppel.afiliaciontrabajador.Utilerias.writeLog;

public class WebVisorSolConstancia extends AppCompatActivity implements View.OnClickListener{
    private String ipAddress, ipServidor;
    private int iEmpleado;
    private String DIR_ACTUAL;
    private LinearLayout lyButtons;
    private ImageView pdfView;
    private int REQ_WIDTH;
    private Button btnRegresar;
    private String sUrl;
    private String ipOffline = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_visor_sol_constancia);

        Intent intent = getIntent();
        getParameters(intent);

        DIR_ACTUAL= Environment.getExternalStorageDirectory().toString()+ "/solicitudconstancias";

        getScreenDimension();

        setComponentes();
        setListeners();

    }

    public void setComponentes(){

        pdfView = findViewById(R.id.pdfViewSol);
        lyButtons = findViewById(R.id.ly_buttons_sol);
        btnRegresar = findViewById(R.id.btn_regresar_touch);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == btnRegresar.getId()){
            File file = new File(DIR_ACTUAL + "/solconstancia.pdf");
            file.delete();
            finish();
        }

    }

    public void setListeners(){

        /* OnClickListener */
        btnRegresar.setOnClickListener(this);

    }

    public void getScreenDimension(){

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getRealSize(size);
        } catch (NoSuchMethodError err) {
            display.getSize(size);
        }

        REQ_WIDTH = size.x;

    }

    private void mostrarPDF(){

        File directory = new File(DIR_ACTUAL);

        // filePath represent path of Pdf document on storage
        File file = new File(directory, "solconstancia.pdf");

        // FileDescriptor for file, it allows you to close file when you are done with it
        ParcelFileDescriptor mFileDescriptor = null;
        try {
            mFileDescriptor = ParcelFileDescriptor.open(file,
                    ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (FileNotFoundException e) {
            writeLog("[FormatoEnrolActivity][mostrarPDF] FileNotFoundException"  + e.getStackTrace());

            e.printStackTrace();
        }

        // PdfRenderer enables rendering a PDF document
        PdfRenderer mPdfRenderer = null;
        try {
            assert mFileDescriptor != null;
            mPdfRenderer = new PdfRenderer(mFileDescriptor);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        // Open page with specified index
        assert mPdfRenderer != null;
        PdfRenderer.Page mCurrentPage = mPdfRenderer.openPage(0);

        int pageWidth = mCurrentPage.getWidth();
        int pageHeight = mCurrentPage.getHeight();
        float scale = (float) REQ_WIDTH / pageWidth;

        pageWidth  = (int) (pageWidth * scale);
        pageHeight = (int) (pageHeight * scale);

        Bitmap bitmap = Bitmap.createBitmap(pageWidth, pageHeight,
                Bitmap.Config.ARGB_8888);

        // Pdf page is rendered on Bitmap
        mCurrentPage.render(bitmap, null, null,
                PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        // Set rendered bitmap to ImageView (pdfView in my case)
        pdfView.setImageBitmap(bitmap);

        mCurrentPage.close();
        mPdfRenderer.close();
        try {
            mFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(WebVisorSolConstancia.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "solicitudconstancias");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }

            try{
                FileDownloader.downloadFile(fileUrl, pdfFile);
            }catch (Exception e){
                writeLog("DownloadFile Exception " + e.getStackTrace().toString());
                finalizarActividadError();
            }


            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            this.progressDialog.dismiss();
            mostrarPDF();
        }



    }

    public void getParameters(Intent intent){

        MessageDialog aviso;

        if(!intent.hasExtra(ACT_AFILIACION_NUM)){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

        try{
            ipAddress = intent.getStringExtra(CONFIG_KEY1);
            ipOffline = intent.getStringExtra("ipOffline");
            iEmpleado = intent.getIntExtra(ACT_AFILIACION_NUM, 0);
            sUrl = intent.getStringExtra("url");

            writeLog("URL Descargar Doc " + sUrl);

            sUrl = sUrl.replace("http://","");
            sUrl = sUrl.replace("https://","");

            int inicio = sUrl.indexOf("/");

            writeLog("URL INDEX " + inicio);

            sUrl = sUrl.substring(inicio);

            writeLog("URL RECORTADA " + sUrl);
            writeLog("URL COMPUESTA " + ipOffline + sUrl);

            new WebVisorSolConstancia.DownloadFile().execute(ipOffline + sUrl, "solconstancia.pdf");

        }
        catch (NullPointerException e){
            aviso = new MessageDialog();
            aviso.setMensaje(getResources().getString(R.string.msn_err_param));
            aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                    (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }

    }
    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finishAndRemoveTask();
    }

    public void finalizarActividadError(){
        setResult(Activity.RESULT_CANCELED);
        MessageDialog aviso;
        aviso = new MessageDialog();
        aviso.setMensaje(getResources().getString(R.string.msn_err_param_url));
        aviso.setPositiveButton(getResources().getString(R.string.opt_aceptar),
                (dialogInterface, i) -> finalizarActividad());
        showMessage(this, aviso);
        finishAndRemoveTask();
    }

    @Override //SE CANCELA CUALQUIER ACCIÓN CON EL BOTÓN DE REGRESAR.
    public void onBackPressed() { }

}
