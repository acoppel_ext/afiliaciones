package com.aforecoppel.afiliaciontrabajador.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergio Gil (AP Interfaces) on 13/07/19.
 */

public class FolioEnrolamiento {

    @SerializedName("estatus")
    public Integer estatus;
    @SerializedName("descripcion")
    public String descripcion;
    @SerializedName("respuesta")
    public String respuesta;

}
