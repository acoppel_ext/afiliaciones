package com.aforecoppel.afiliaciontrabajador.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergio Gil (AP Interfaces) on 26/07/19.
 */

public class infoConexion {

    @SerializedName("iFolio")
    public String iFolio;
    @SerializedName("iTipoConexion")
    public String iTipoConexion;

    public infoConexion(String iFolio, String iTipoConexion) {
        this.iFolio = iFolio;
        this.iTipoConexion = iTipoConexion;
    }

    //creates a json-format string
    public static String createConexion(String iFolio, String iTipoConexion){
        String info = "{\"iFolio\":\""+iFolio+"\",\"iTipoConexion\":\""+iTipoConexion+"\"}";

        return info;
    }
}
