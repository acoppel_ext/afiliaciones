package com.aforecoppel.afiliaciontrabajador.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergio Gil (AP Interfaces) on 14/12/18.
 */

public class infoFolio {

    @SerializedName("iFolioSolicitud")
    public String iFolioSolicitud;
    @SerializedName("iFolioEnrolamiento")
    public String iFolioEnrolamiento;

    public infoFolio(String iFolioSolicitud, String iFolioEnrolamiento) {
        this.iFolioSolicitud = iFolioSolicitud;
        this.iFolioEnrolamiento = iFolioEnrolamiento;
    }

    //creates a json-format string
    public static String createFolioEnrolameinto(String iFolioSolicitud, String iFolioEnrolamiento){
        String info = "{\"iFolioSolicitud\":\""+iFolioSolicitud+"\",\"iFolioEnrolamiento\":\""+iFolioEnrolamiento+"\"}";

        return info;
    }
}
